<?$this->load->view('elements/header');?>

<div class="xcontents xlogin">
  <div class="xmain">
    <h1>ログイン</h1>
    <?=form_open();?>
      <dl>
        <dd>
          <?=inp_text("email", null, 'class="form-control" placeholder="メールアドレス" autofocus required');?>
          <?=form_error("email");?>
          <?if ($show_error) {?><p class="xerrors">メールアドレスまたは、パスワードが間違っています</p><?}?>
        </dd>
        <dd>
          <?=inp_password("password", null, 'class="form-control" placeholder="パスワード" required');?>
          <?=form_error("password");?>
        </dd>
      </dl>
      <div class="xsubmit">
        <?=form_submit("submit", "ログイン", 'class="btn btn-primary"');?>
      </div>
    </form>
  </div>
</div>

<?$this->load->view('elements/footer');?>
