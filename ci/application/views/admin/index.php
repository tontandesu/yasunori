<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?=$title;?></title>
<meta name="robots" content="noindex">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1">
<meta name="csrf-token-name" content="<?= $this->security->get_csrf_token_name();?>">
<meta name="csrf-token" content="<?= $this->security->get_csrf_hash();?>">
<link rel="shortcut icon" href="/favicon.ico">
<?if ($apple_touch_icon) {?>
<link rel="apple-touch-icon" href="<?=$apple_touch_icon;?>">
<?}?>
<?if ($assets_id) {?>
<link rel="stylesheet" href="/css/<?=$assets_id;?>.css?<?=dvn();?>" />
<?}?>
</head>
<body>
  <main role="main" id="app"><router-view></router-view></main>
<?if ($assets_id) {?>
<script src="/js/<?=$assets_id;?>.js?<?=dvn();?>"></script>
<?}?>
</body>
</html>
