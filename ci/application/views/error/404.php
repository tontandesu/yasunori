<?$this->load->view('elements/header');?>
<div class="xerror_page">
  <h2>404</h2>
  <p class="xsub_title">お探しのページは見つかりませんでした</p>
  <p class="xtext">このページは移動もしくは削除された可能性があります。</p>
  <a href="/" class="btn btn-primary">トップページへ戻る</a>
</div>
<?$this->load->view('elements/footer');?>
