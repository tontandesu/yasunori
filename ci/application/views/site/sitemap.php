<?php
header('Content-Type: text/xml;charset=iso-8859-1');
echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";
?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc><?=get_http_url();?></loc>
  </url>
  <url>
    <loc><?=get_http_url()."/videos";?></loc>
  </url>
  <?foreach($videos as $video) {?>
  <url>
    <loc><?=get_http_url()."/video/{$video['id']}";?></loc>
    <lastmod><?=date('Y-m-d',  strtotime($video['created_at']));?></lastmod>
  </url>
  <?}?>
</urlset>
