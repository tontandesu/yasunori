<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?=$title;?></title>
<?if ($noindex) {?>
<meta name="robots" content="noindex">
<?}?>
<?if ($noreferrer) {?>
<meta name="referrer" content="no-referrer">
<?}?>
<meta name="keywords" content="<?=$keywords;?>">
<meta name="description" content="<?=$description;?>">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1">
<meta name="csrf-token-name" content="<?= $this->security->get_csrf_token_name();?>">
<meta name="csrf-token" content="<?= $this->security->get_csrf_hash();?>">
<link rel="shortcut icon" href="/favicon.ico">
<?if ($apple_touch_icon) {?>
<link rel="apple-touch-icon" href="<?=$apple_touch_icon;?>">
<?}?>
<?if ($assets_id) {?>
<link rel="stylesheet" href="/css/<?=$assets_id;?>.css?<?=dvn();?>" />
<?}?>
<?if ($eyecatch) {?>
<meta property="og:image" content="<?=$eyecatch;?>" />
<?}?>
<?if ($rss_title && $rss_url) {?>
<link rel="alternate" type="application/rss+xml" title="<?=$rss_title;?>" href="<?=$rss_url;?>" />
<?}?>
<?if ($free_header) echo r($free_header);?>
</head>
<?if ($is_eyecatch) {?>
<body class="xeye">
<?} else {?>
<body>
<?}?>
<header>
  <div class="xinner">
    <?if ($is_h1) {?>
      <h1><a href="/" class="xlogo"><?=$service_name;?></a></h1>
    <?} else {?>
      <a href="/" class="xlogo"><?=$service_name;?></a>
    <?}?>
  </div>
</header>
<main role="main">
