<div class="xside">
  <?if ($recommend_tags) {?>
  <section>
    <p class="xt">人気キーワード</p>
    <ul>
      <?foreach ($recommend_tags as $tag) {?>
        <a href="<?="/videos/tag/{$tag}";?>"><?=$tag;?></a>
      <?}?>
    </ul>
  </section>
  <?}?>

  <section>
    <p class="xt">タグ一覧</p>
    <ul>
      <?foreach ($tags as $tag) {?>
        <a href="<?="/videos/tag/{$tag}";?>"><?=$tag;?></a>
      <?}?>
    </ul>
  </section>
</div>
