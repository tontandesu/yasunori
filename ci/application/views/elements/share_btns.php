<?if (empty($query)) $query = '';?>
<div class="xshare_btns">
  <a href="" target="_blank" class="xshare_btn _share_facebook" data-query="<?=$query;?>">
    <?$this->load->view('elements/img', [ 'src' => '/img/icons/facebook_icon.svg', 'alt' => 'フェイスブック' ]);?>
  </a>
  <a href="" target="_blank" class="xshare_btn _share_twitter" data-query="<?=$query;?>">
    <?$this->load->view('elements/img', [ 'src' => '/img/icons/twitter.svg', 'alt' => 'ツイッター' ]);?>
  </a>
  <a href="" class="xshare_btn _share_line" data-query="<?=$query;?>">
    <?$this->load->view('elements/img', [ 'src' => '/img/icons/line.svg', 'alt' => 'ライン' ]);?>
  </a>
  <a href="" target="_blank" class="xshare_btn _share_hatena" data-query="<?=$query;?>">
    <?$this->load->view('elements/img', [ 'src' => '/img/icons/hatena_icon.svg', 'alt' => 'はてな' ]);?>
  </a>
</div>
