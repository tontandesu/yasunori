<?php

class Lvideo
{
  private $_CI;

  public $status = [
    ['id' => 0, 'name' => 'disabled'], // 無効
    ['id' => 1, 'name' => 'enabled'], // 有効
  ];

  public function __construct()
  {
    $this->_CI =& get_instance();
  }

  public function status_id($name)
  {
    return items_find($this->status, 'name', $name)['id'];
  }

  public function status_name($id)
  {
    return items_find($this->status, 'id', $id)['name'];
  }

  public function get_videos($params, $page = 1, $limit = PRE_PAGE)
  {
    $status = selection($params, 'status', 'enabled');
    $selects = selection($params, 'selects', '*');
    $sort = selection($params, 'sort', 'seq desc');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('videos');
    if ($status) $this->_CI->db->where('status', $this->status_id($status));
    $tmpdb = clone $this->_CI->db;
    $total = $tmpdb->count_all_results();
    $tmpdb = null;

    $this->_CI->db->order_by($sort);
    $this->_CI->db->limit($limit, get_offset($limit, $page));
    $result = array_optimize($this->_CI->db->get()->result_array());
    // write_log("debug", $this->_CI->db->last_query());
    // print_r($this->_CI->db->last_query());exit;

    return [$total, $result];
  }

  public function get_video_min($params)
  {
    $video_seq = selection($params, 'video_seq');
    $video_id = selection($params, 'video_id');
    $url = selection($params, 'url');

    $this->_CI->db->select('seq, id, url');
    $this->_CI->db->from('videos');
    if ($video_seq) $this->_CI->db->where('seq', $video_seq);
    if ($video_id) $this->_CI->db->where('id', $video_id);
    if ($url) $this->_CI->db->where('url', $url);
    $result = array_optimize($this->_CI->db->get()->row_array());
    return $result;
  }

  public function get_video($params)
  {
    $video_seq = selection($params, 'video_seq');
    $video_id = selection($params, 'video_id');
    $status = selection($params, 'status');
    $selects = selection($params, 'selects', 'videos.*');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('videos');
    if ($video_seq) $this->_CI->db->where('videos.seq', $video_seq);
    if ($video_id) $this->_CI->db->where('videos.id', $video_id);
    if ($status) $this->_CI->db->where('videos.status', $this->status_id($status));
    $result = array_optimize($this->_CI->db->get()->row_array());
    if ($result) {
      $result['tags'] = $this->get_tags(['video_seq' => $result['seq']]);
      $result['status'] = $this->status_name($result['status']);
    }
    return $result;
  }

  public function save_video($values)
  {
    $video_seq = selection($values, 'seq');
    $this->_CI->db->trans_begin();
    if ($video_seq) {
      $video_seq = $this->update_video($values);
      // TODO: site_videoも更新する id url eyecatch status
    } else {
      $video_seq = $this->create_video($values);
    }
    $this->update_video_tag_map($video_seq, $values['tags']);

    if ($this->_CI->db->trans_status() === false) {
      $this->_CI->db->trans_rollback();
      throw new Exception;
    }
    $this->_CI->db->trans_commit();
    return $video_seq;
  }

  public function create_video($values)
  {
    $id = ($values['id'])? strtolower($values['id']) : uniqid('v');
    $data = [
      'id' => $id,
      'title' => $values['title'],
      'description' => $values['description'],
      'keywords' => $values['keywords'],
      'url' => $values['url'],
      'eyecatch' => $values['eyecatch'],
      'comment' => $values['comment'],
      'play_count' => $values['play_count'],
      'good_count' => $values['good_count'],
      'bad_count' => $values['bad_count'],
      'status' => $this->status_id($values['status'])
    ];
    $this->_CI->db->insert('videos', $data);
    return $this->_CI->db->insert_id();
  }

  public function update_video($values)
  {
    $seq = $values['seq'];
    $id = ($values['id'])? strtolower($values['id']) : uniqid('v');
    $data = [
      'id' => $id,
      'title' => $values['title'],
      'description' => $values['description'],
      'keywords' => $values['keywords'],
      'url' => $values['url'],
      'eyecatch' => $values['eyecatch'],
      'comment' => $values['comment'],
      'play_count' => $values['play_count'],
      'good_count' => $values['good_count'],
      'bad_count' => $values['bad_count'],
      'status' => $this->status_id($values['status'])
    ];
    $res = $this->_CI->db->update('videos', $data, ['seq' => $seq]);
    return ($res)? $seq : null;
  }

  public function update_video_tag_map($video_seq, $tags)
  {
    $tags = explode(',', $tags);
    $this->_CI->db->delete('video_tag_map', ['video_seq' => $video_seq]);
    foreach ($tags as $tag_seq) {
      $this->_CI->db->insert('video_tag_map', ['video_seq' => $video_seq, 'tag_seq' => $tag_seq]);
    }
  }

  public function get_tags($params = [])
  {
    $video_seq = selection($params, 'video_seq');
    $sort = selection($params, 'sort', 'tags.kana asc');

    $this->_CI->db->select('tags.seq, tags.name');
    $this->_CI->db->from('tags');
    if ($video_seq) {
      $this->_CI->db->join('video_tag_map', 'video_tag_map.tag_seq = tags.seq');
      $this->_CI->db->where('video_tag_map.video_seq', $video_seq);
    }
    $this->_CI->db->order_by($sort);
    $result = array_optimize($this->_CI->db->get()->result_array());
    return $result ?: [];
  }

  public function delete_video($video_seq)
  {
    $this->_CI->db->trans_begin();
    $this->_CI->db->delete('videos', ['seq' => $video_seq]);
    $this->_CI->db->delete('video_tag_map', ['video_seq' => $video_seq]);
    $this->_CI->db->delete('site_videos', ['video_seq' => $video_seq]);
    if ($this->_CI->db->trans_status() === false) {
      $this->_CI->db->trans_rollback();
      return false;
    }
    $this->_CI->db->trans_commit();
    return true;
  }
}
