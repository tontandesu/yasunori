<?php

class Lsite
{
  private $_CI;

  public $status = [
    ['id' => 0, 'name' => 'disabled'], // 無効
    ['id' => 1, 'name' => 'enabled'], // 有効
  ];

  public function __construct()
  {
    $this->_CI =& get_instance();
  }

  public function status_id($name)
  {
    return items_find($this->status, 'name', $name)['id'];
  }

  public function status_name($id)
  {
    return items_find($this->status, 'id', $id)['name'];
  }



  public function get_sites($params, $page = 1, $limit = PRE_PAGE)
  {
    $status = selection($params, 'status', 'enabled');
    $selects = selection($params, 'selects', '*');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('sites');
    if ($status) $this->_CI->db->where('status', $this->status_id($status));
    $tmpdb = clone $this->_CI->db;
    $total = $tmpdb->count_all_results();
    $tmpdb = null;

    $this->_CI->db->limit($limit, get_offset($limit, $page));
    $result = array_optimize($this->_CI->db->get()->result_array());
    // write_log("debug", $this->_CI->db->last_query());

    return [$total, $result];
  }

  public function get_site_min($params)
  {
    $site_seq = selection($params, 'site_seq');
    $domain = selection($params, 'domain');

    $this->_CI->db->select('seq, domain');
    $this->_CI->db->from('sites');
    if ($site_seq) $this->_CI->db->where('seq', $site_seq);
    if ($domain) $this->_CI->db->where('domain', $domain);
    $result = array_optimize($this->_CI->db->get()->row_array());
    return $result;
  }

  public function get_site($params)
  {
    // サイトの紐づくタグも取得
    $site_seq = selection($params, 'site_seq');
    $domain = selection($params, 'domain');
    $status = selection($params, 'status', 'enabled');
    $selects = selection($params, 'selects', 'sites.*');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('sites');
    if ($site_seq) $this->_CI->db->where('sites.seq', $site_seq);
    if ($domain) $this->_CI->db->where('sites.domain', $domain);
    if ($status) $this->_CI->db->where('sites.status', $this->status_id($status));
    $result = array_optimize($this->_CI->db->get()->row_array());
    if ($result) {
      $result['status'] = $this->status_name($result['status']);
    }
    return $result;
  }

  public function save_site($values)
  {
    $site_seq = selection($values, 'seq');

    // TODO: NGタグ置換処理

    $this->_CI->db->trans_begin();
    if ($site_seq) {
      $site_seq = $this->update_site($values);
    } else {
      $site_seq = $this->create_site($values);
    }
    if ($this->_CI->db->trans_status() === false) {
      $this->_CI->db->trans_rollback();
      throw new Exception;
    }
    $this->_CI->db->trans_commit();
    return $site_seq;
  }

  public function create_site($values)
  {
    $data = [
      'domain' => $values['domain'],
      'name' => $values['name'],
      'kana' => $values['kana'],
      'copy' => $values['copy'],
      'description' => $values['description'],
      'keywords' => $values['keywords'],
      'eyecatch' => $values['eyecatch'],
      'theme' => $values['theme'],
      'script' => $values['script'],
      'status' => $this->status_id($values['status'])
    ];
    $this->_CI->db->insert('sites', $data);
    return $this->_CI->db->insert_id();
  }

  public function update_site($values)
  {
    $site_seq = $values['seq'];
    $data = [
      'domain' => $values['domain'],
      'name' => $values['name'],
      'kana' => $values['kana'],
      'copy' => $values['copy'],
      'description' => $values['description'],
      'keywords' => $values['keywords'],
      'eyecatch' => $values['eyecatch'],
      'theme' => $values['theme'],
      'script' => $values['script'],
      'status' => $this->status_id($values['status'])
    ];
    $res = $this->_CI->db->update('sites', $data, ['seq' => $site_seq]);
    return ($res)? $site_seq : null;
  }

  // public function get_tags($params = [])
  // {
  //   $site_seq = selection($params, 'site_seq');
  //   $sort = selection($params, 'sort', 'tags.kana asc');

  //   $this->_CI->db->select('tags.seq, tags.name');
  //   $this->_CI->db->from('tags');
  //   if ($site_seq) {
  //     $this->_CI->db->join('site_tag_map', 'site_tag_map.tag_seq = tags.seq');
  //     $this->_CI->db->where('site_tag_map.site_seq', $site_seq);
  //   }
  //   $this->_CI->db->order_by($sort);
  //   $result = array_optimize($this->_CI->db->get()->result_array());
  //   return $result ?: [];
  // }

  public function delete_site($site_seq)
  {
    $this->_CI->db->trans_begin();
    $this->_CI->db->delete('sites', ['seq' => $site_seq]);
    $this->_CI->db->delete('site_videos', ['site_seq' => $site_seq]);
    if ($this->_CI->db->trans_status() === false) {
      $this->_CI->db->trans_rollback();
      return false;
    }
    $this->_CI->db->trans_commit();
    return true;
  }
}
