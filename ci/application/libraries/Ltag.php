<?php

class Ltag
{
  private $_CI;

  public function __construct()
  {
    $this->_CI =& get_instance();
  }

  public function get_tags($params = [])
  {
    $selects = selection($params, 'selects', '*');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('tags');
    $result = array_optimize($this->_CI->db->get()->result_array());
    return $result;
  }

  public function get_video_tags($params = [])
  {
    $site_seq = selection($params, 'site_seq');
    $done = selection($params, 'done', 1);

    $this->_CI->db->distinct();
    $this->_CI->db->select('tags.name, tags.recommend');
    $this->_CI->db->from('tags');
    $this->_CI->db->join('video_tag_map', 'video_tag_map.tag_seq = tags.seq');
    $this->_CI->db->join('site_videos', 'site_videos.video_seq = video_tag_map.video_seq');
    if ($site_seq) $this->_CI->db->where('site_videos.site_seq', $site_seq);
    if ($done) $this->_CI->db->where('site_videos.done', $done);
    $result = array_optimize($this->_CI->db->get()->result_array());
    return $result;
  }

  public function get_tag($params)
  {
    $tag_seq = selection($params, 'tag_seq');
    $name = selection($params, 'name');

    $this->_CI->db->select('*');
    $this->_CI->db->from('tags');
    if ($tag_seq) $this->_CI->db->where('seq', $tag_seq);
    if ($name) $this->_CI->db->where('name', $name);
    $result = array_optimize($this->_CI->db->get()->row_array());
    return $result;
  }

  public function save_tag($values)
  {
    $tag_seq = selection($values, 'seq');
    $this->_CI->db->trans_begin();
    if ($tag_seq) {
      $tag_seq = $this->update_tag($values);
    } else {
      $tag_seq = $this->create_tag($values);
    }
    if ($this->_CI->db->trans_status() === false) {
      $this->_CI->db->trans_rollback();
      throw new Exception;
    }
    $this->_CI->db->trans_commit();
    return $tag_seq;
  }

  public function create_tag($values)
  {
    $data = [
      'name' => $values['name'],
      'kana' => $values['kana'],
      'recommend' => $values['recommend']
    ];
    $this->_CI->db->insert('tags', $data);
    return $this->_CI->db->insert_id();
  }

  public function update_tag($values)
  {
    $tag_seq = $values['seq'];
    $data = [
      'name' => $values['name'],
      'kana' => $values['kana'],
      'recommend' => $values['recommend']
    ];
    $res = $this->_CI->db->update('tags', $data, ['seq' => $tag_seq]);
    return ($res)? $tag_seq : null;
  }

  public function delete_tag($tag_seq)
  {
    $this->_CI->db->trans_begin();
    $this->_CI->db->delete('tags', ['seq' => $tag_seq]);
    $this->_CI->db->delete('video_tag_map', ['tag_seq' => $tag_seq]);
    if ($this->_CI->db->trans_status() === false) {
      $this->_CI->db->trans_rollback();
      return false;
    }
    $this->_CI->db->trans_commit();
    return true;
  }
}
