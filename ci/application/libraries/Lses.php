<?php

require PATH_PJ . 'vendor/autoload.php';

use Aws\CommandInterface;
use Aws\CommandPool;
use Aws\Exception\AwsException;
use Aws\ResultInterface;
use Aws\Ses\SesClient;

class Lses
{
    private $_CI;

    private $client;

    // メール情報
    private $info = [
        'source' => '',
        'subject' => '',
        'body_text' => '',
        'body_html' => '',
    ];

    // 送り先情報
    private $destinations = [];

    public function __construct()
    {
        // KEYはQA専用になっている
        $this->_CI =& get_instance();
        $this->client = new SesClient([
            'version'     => 'latest',
            'region'      => 'us-east-1',
            'credentials' => [
                'key'    => AWS_ACCESS_KEY,
                'secret' => AWS_SECRET_KEY,
            ],
        ]);
    }

    /**
     * メール情報を設定
     *
     * @param [type] $info
     * @return void
     */
    public function set_info($info)
    {
        $this->info = array_merge($this->info, $info);
        if (!$this->info['source']) $this->info['source'] = $this->get_address(FROM_EMAIL, FROM_EMAIL_NAME);
    }

    /**
     * 送信先を初期化する
     *
     * @return void
     */
    public function reset_destination()
    {
        $this->destinations = [];
    }

    /**
     * 送信先を追加
     *
     * @param [type] $desti
     * @param array $data
     * @return void
     */
    public function add_destination($desti)
    {
        $destination = [
            'to' => selection($desti, 'to', []),
            'cc' => selection($desti, 'cc', []),
            'bcc' => selection($desti, 'bcc', []),
            'data' => selection($desti, 'data', []),
        ];
        $this->destinations[] = $destination;
    }

    public function get_address($email, $name='')
    {
        return ($name)? mb_encode_mimeheader($name, 'UTF-7', 'Q') . " <{$email}>" : $email;
    }

    public function send()
    {
        // TODO ページング
        $i = 100;
        $commands = [];
        foreach ($this->destinations as $destination) {
            $ses_setting = [
                'x-message-id' => $i,
                'Source' => $this->info['source'],
                'Destination' => [
                    'ToAddresses' => $destination['to'],
                    'CcAddresses' => $destination['cc'],
                    'BccAddresses' => $destination['bcc'],
                ],
                'Message' => [
                    'Subject' => [
                        'Charset' => 'UTF-8',
                        'Data' => $this->info['subject']
                    ],
                    'Body' => [
                        'Html' => [
                            'Charset' => 'UTF-8',
                            'Data' => $this->info['body_html']
                        ],
                        'Text' => [
                            'Charset' => 'UTF-8',
                            'Data' => $this->info['body_text']
                        ],
                    ],
                ]
            ];

            // TEXTがある場合HTMLを削除する
            if ($this->info['body_text']) {
                unset($ses_setting['Message']['Body']['Html']);
            }

            $commands[] = $this->client->getCommand('SendEmail', $ses_setting);
            $i++;
        }

        try {
            $timeStart = microtime(true);
            //$this->_CI->slog->info('START SES2 SEND');

            $pool = new CommandPool($this->client, $commands, [
                'concurrency' => 10,
                'before' => function (CommandInterface $cmd, $iteratorId) {
                    // $a = $cmd->toArray();
                    // echo sprintf('About to send %d: %s' . PHP_EOL, $iteratorId, $a['Destination']['ToAddresses'][0]);
                },
                'fulfilled' => function (ResultInterface $result, $iteratorId) use ($commands) {
                    // echo sprintf(
                    //     'Completed %d: %s' . PHP_EOL,
                    //     $commands[$iteratorId]['x-message-id'],
                    //     $commands[$iteratorId]['Destination']['ToAddresses'][0]
                    // );
                },
                'rejected' => function (AwsException $reason, $iteratorId) use ($commands) {
                    // echo sprintf(
                    //     'Failed %d: %s' . PHP_EOL,
                    //     $commands[$iteratorId]['x-message-id'],
                    //     $commands[$iteratorId]['Destination']['ToAddresses'][0]
                    // );

                    // $this->_CI->slog->error($reason->getAwsErrorMessage(), ["email" => $commands[$iteratorId]['Destination']['ToAddresses'][0]]);
                },
            ]);
            // アドレスエラーはとれないっぽい
            $promise = $pool->promise();
            $promise->wait();

            $timeEnd = microtime(true);
            // log_message("info", "END SES SEND");
            //$this->_CI->slog->info("END SES SEND", ["seconds" => $timeEnd - $timeStart]);
            return true;
        } catch (Exception $e) {
            log_message('error', $e->getMessage());
            return false;
        }
    }
}
