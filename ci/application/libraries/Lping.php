<?php

class Lping
{
  public function send($site_title, $page_url)
  {
    if (is_dev()) return;
    $site_title= htmlspecialchars($site_title);
    $content = <<<XML
<?xml version='1.0'?>
<methodCall>
<methodName>weblogUpdates.ping</methodName>
<params>
  <param><value>{$site_title}</value></param>
  <param><value>{$page_url}</value></param>
</params>
</methodCall>
XML;
    $header = array('Content-Length: '.strlen($content), 'Content-Type: text/xml');
    $urls = array(
      "http://blogsearch.google.co.jp/ping/RPC2"
      , "http://blogsearch.google.com/ping/RPC2"
      , "http://pingoo.jp/ping/"
    );
    foreach ($urls as $url) {
      try{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $res = curl_exec($ch);
        curl_close($ch);
      }catch(Exception $e){
        // log
      }
    }
    return true;
  }
}
