<?php

class Luser
{
  private $_CI;
  private $columns = ['user_id', 'email', 'password', 'permission', 'nickname', 'icon', 'message', 'status', 'provisional', 'updated_at'];
  // TODO: 取得時にnameに変換、保存時にidに変換 新しいFWではココらへん自動でやってくれるはず
  public $status = [
    ['id' => 0, 'name' => 'disabled'], // 無効
    ['id' => 1, 'name' => 'enabled'] // 有効
  ];
  public $permission = [
    ['id' => 1, 'name' => 'normal'], // 通常会員
    ['id' => 2, 'name' => 'editor'], // 編集者
    ['id' => 9, 'name' => 'admin'] // 管理者
  ];

  public function __construct()
  {
    $this->_CI =& get_instance();
    $this->_CI->load->library('encryption');
  }

  public function status_id($name)
  {
    return items_find($this->status, 'name', $name)['id'];
  }

  public function status_name($id)
  {
    return items_find($this->status, 'id', $id)['name'];
  }

  public function permission_id($name)
  {
    return items_find($this->permission, 'name', $name)['id'];
  }

  public function permission_name($id)
  {
    return items_find($this->permission, 'id', $id)['name'];
  }

  public function login($user)
  {
    $user_data = [
      'user_seq' => $user['seq'],
      'user_id' => $user['id'],
      'display_name' => $user['nickname'] ?: $user['id'],
      'icon' => $user['icon'],
      'permission' => $user['permission'],
    ];
    // $this->_CI->session->unset_userdata('user');
    $this->_CI->session->set_userdata('user', $user_data);
  }

  public function logout()
  {
    $this->_CI->session->sess_destroy();
  }

  public function is_login($approved_permissions = [], $is_redirect = true)
  {
    $result = true;
    $user = $this->get_login_user();

    if ($user) {
      $is_permission = $this->is_permission($approved_permissions, $user['permission']);
      if (!$is_permission && $is_redirect) show_error('Permission Error.', 403);
      if (!$is_permission) $result = false;
    } else {
      $result = false;
      if ($is_redirect) {
        redirect('/login', 'refresh');
      }
    }
    return $result;
  }

  public function get_current_user_seq()
  {
    $user = $this->_CI->session->userdata('user');
    if (!$user) return null;
    return $user['user_seq'];
  }

  public function get_login_user()
  {
    return $this->_CI->session->userdata('user') ?: [];
  }

  public function is_current_user($target_seq, $current_user)
  {
    return !(!$current_user || $current_user['user_seq'] != $target_seq);
  }

  public function is_permission($approved_permissions, $user_permission)
  {
    $result = true;
    if ($approved_permissions && !in_array($user_permission, $approved_permissions)) {
      $result = false;
    }
    return $result;
  }

  public function get_user($params)
  {
    $user_seq = selection($params, 'user_seq');
    $user_id = selection($params, 'user_id');
    $email = selection($params, 'email');
    $status = selection($params, 'status', 'enabled');
    $selects = selection($params, 'selects', '*');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('user');
    if ($user_seq) $this->_CI->db->where('seq', $user_seq);
    if ($user_id) $this->_CI->db->where('id', $user_id);
    if ($email) $this->_CI->db->where('email', $email);
    if ($status) $this->_CI->db->where('status', $this->status_id($status));
    $result = array_optimize($this->_CI->db->get()->row_array());
    if ($result) {
      if (isset($result['permission'])) $result['permission'] = $this->permission_name($result['permission']);
      if (isset($result['status'])) $result['status'] = $this->status_name($result['status']);
      if (isset($result['nickname']) && isset($result['id'])) $result['display_name'] = $result['nickname'] ?: $result['id'];
      if (isset($result['password'])) $result['password'] = $this->_CI->encryption->decrypt($result['password']);
    }
    return $result;
  }

  public function delete_user($user_seq)
  {
    return $this->_CI->db->delete('user', ['user_seq' => $user_seq]);
  }

  public function get_user_list($params, $page = 1, $limit = PRE_PAGE)
  {
    $result = [];
    $this->_CI->db->select('*');
    $this->_CI->db->from('user');
    $result = array_optimize($this->_CI->db->get()->result_array()) ?: [];
    return array_map(function ($user) {
      $user['status'] = $this->status_name($user['status']);
      return $user;
    }, $result);
  }

  public function create($values)
  {
    $data = [
      'id' => strtolower($values['user_id']),
      'email' => $values['email'],
      'password' => $this->_CI->encryption->encrypt($values['password']),
      'permission' => $this->permission_id($values['permission']),
      'status' => $this->status_id($values['status'])
    ];
    $this->_CI->db->insert('user', $data);
    $user_seq = $this->_CI->db->insert_id();
    return $user_seq;
  }

  public function update($user_seq, $params)
  {
    $values = array_intersect_key($params, array_flip($this->columns));
    if (!empty($values['password'])) $values['password'] =  $this->_CI->encryption->encrypt($values['password']);
    return $this->_CI->db->update('user', $values, ['user_seq' => $user_seq]);
  }

  // public function edit($values)
  // {
  //   $in = [];
  //   $in['user_id'] = $values['user_id'];
  //   $in['email'] = $values['email'];
  //   if($values['password']) $in['password'] = $this->_CI->encryption->encrypt($values['password']);
  //   $in['permission'] = $values['permission'];
  //   $in['nickname'] = $values['nickname'];
  //   $in['status'] = $values['status'];
  //   $this->_CI->db->update('user', $in, ['user_seq' => $values['user_seq']]);
  //   return true;
  // }

  public function delete_password_reset($token)
  {
    return $this->_CI->db->delete('user_password_reset', ['token' => $token]);
  }

  public function get_password_reset($params)
  {
    $token = selection($params, 'token');
    $expire_at = selection($params, 'expire_at', get_datetime('-1 hour'));
    $selects = selection($params, 'selects', 'token, user_seq');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('user_password_reset');
    if ($token) $this->_CI->db->where('token', $token);
    if ($expire_at) $this->_CI->db->where('issued_at >', $expire_at);
    $result = $this->_CI->db->get()->row_array();
    return array_optimize($result);
  }

  public function password_link_send($user_seq, $email)
  {
    $data = [
      'token' => get_token(),
      'user_seq' => $user_seq
    ];
    $this->_CI->db->insert('user_password_reset', $data);

    $reset_url = get_http_url() . "/password/input/{$data['token']}";
    $title = 'パスワード再設定のご案内';
    if (is_dev()) $title = '[TEST]' . $title;
    $footer = EMAIL_FOOTER;
    $body = <<<TEXT
パスワードの再設定は以下URLのページより、1時間以内に行なってください。
{$reset_url}

{$footer}
TEXT;

    $this->_CI->lses->set_info([ 'subject' => $title, 'body_text' => $body ]);
    $this->_CI->lses->add_destination([
      'to' => [$email]
    ]);
    if (!$this->_CI->lses->send()) {
      // TODO: スラック通知とか
    }
  }

  public function password_update($user_seq, $password, $token)
  {
    $this->_CI->db->trans_begin();
    $this->update($user_seq, ['password' => $password]);
    $this->delete_password_reset($token);
    if ($this->_CI->db->trans_status() === false) {
      $this->_CI->db->trans_rollback();
      return false;
    }
    $this->_CI->db->trans_commit();
    return true;
  }

  public function send_email_reset_conf($user_seq, $email)
  {
    $data = [
      'token' => get_token(),
      'user_seq' => $user_seq,
      'email' => $email
    ];
    $this->_CI->db->insert('user_email_auth', $data);

    $reset_url = get_http_url() . "/email-reset/conf/{$data['token']}";
    $title = 'メールアドレス変更方法のご案内';
    if (is_dev()) $title = '[TEST]' . $title;
    $footer = EMAIL_FOOTER;
    $body = <<<TEXT
到着から1時間以内に以下URLにアクセスすると、メールアドレス変更が完了します。
{$reset_url}

{$footer}
TEXT;

    $this->_CI->lses->set_info([ 'subject' => $title, 'body_text' => $body ]);
    $this->_CI->lses->add_destination([
      'to' => [$email]
    ]);
    if (!$this->_CI->lses->send()) {
      // TODO: スラック通知とか
    }
  }

  public function send_email_auth_conf($user_seq, $email)
  {
    $data = [
      'token' => get_token(),
      'user_seq' => $user_seq,
      'email' => $email
    ];
    $this->_CI->db->insert('user_email_auth', $data);

    $reset_url = get_http_url() . "/email-auth/conf/{$data['token']}";
    $title = 'メールアドレス認証方法のご案内';
    if (is_dev()) $title = '[TEST]' . $title;
    $footer = EMAIL_FOOTER;
    $body = <<<TEXT
到着から2週間以内に以下URLにアクセスすると、メールアドレス認証が完了します。
{$reset_url}
もしアクセスがない場合、登録は削除され再登録が必要になりますのでご注意ください。

{$footer}
TEXT;

    $this->_CI->lses->set_info([ 'subject' => $title, 'body_text' => $body ]);
    $this->_CI->lses->add_destination([
      'to' => [$email]
    ]);
    if (!$this->_CI->lses->send()) {
      // TODO: スラック通知とか
    }
  }

  public function get_email_auth($params)
  {
    $token = selection($params, 'token');
    $expire_at = selection($params, 'expire_at', get_datetime('-1 hour'));
    $selects = selection($params, 'selects', 'token, user_seq, email');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('user_email_auth');
    if ($token) $this->_CI->db->where('token', $token);
    if ($expire_at) $this->_CI->db->where('issued_at >', $expire_at);
    $result = $this->_CI->db->get()->row_array();
    return array_optimize($result);
  }

  public function delete_email_auth($token)
  {
    return $this->_CI->db->delete('user_email_auth', ['token' => $token]);
  }

  public function update_email($token)
  {
    $email_auth = $this->get_email_auth(['token' => $token]);
    if (!$email_auth) return false;

    $this->_CI->db->trans_begin();
    $this->update($email_auth['user_seq'], ['email' => $email_auth['email']]);
    $this->delete_email_auth($token);
    if ($this->_CI->db->trans_status() === false) {
      $this->_CI->db->trans_rollback();
      return false;
    }
    $this->_CI->db->trans_commit();
    return true;
  }

  public function update_password($params)
  {
    $res = $this->update($params['user_seq'], ['password' => $params['new_password']]);
    if (!$res) throw new Exception();
  }

  /**
   * 本登録に変更
   *
   * @param [type] $token
   * @return void
   */
  public function to_registration($token)
  {
    $expire_at = get_datetime('-2 week');
    $email_auth = $this->get_email_auth(['token' => $token, 'expire_at' => $expire_at]);
    if (!$email_auth) {
      $this->delete_email_auth($token);
      return false;
    }

    $this->_CI->db->trans_begin();
    $this->update($email_auth['user_seq'], ['provisional' => false]);
    $this->delete_email_auth($token);
    if ($this->_CI->db->trans_status() === false) {
      $this->_CI->db->trans_rollback();
      return false;
    }
    $this->_CI->db->trans_commit();
    return true;
  }
}
