<?php
/**
 * Form_validation 拡張
 */
class  MY_Form_validation extends CI_Form_validation {

	private $_CI;

	public function __construct()
	{
		parent::__construct();

		$this->_CI = &get_instance();
		$this->set_error_delimiters('<p class="x_errors">', '</p>');
	}

	/**
	 * ルールの設定とvalidation実行をする
	 */
	public function run_validation(&$request, $rules)
	{
		$this->set_data($request);

		// set rules
		foreach($rules as $rule){
			call_user_func_array([$this, 'set_rules'], $rule);

			// 穴埋め
			if(!isset($request[$rule[0]])) $request[$rule[0]] = null;
		}

		$this->run();
	}

	/**
	 * エラーチェック
	 */
	public function is_error()
	{
		return (count($this->error_array()));
	}

	/**
	 * エラー配列に追加
	 */
	public function set_error_array($key, $message='')
	{
		$this->_error_array[$key] = $message;
	}

	/**
	 * エラーメッセージを取得
	 * @return number
	 */
	public function get_error_message($rule, $label='', $param='')
	{
		return $this->_build_error_msg($this->_CI->lang->line("form_validation_{$rule}"), $label, $param);
	}

	/**
	 * 日付フォーマットチェック
	 */
	public function date($str, $delim)
	{
		if(!$delim) $delim = '-';

		return $this->regex_match($str, "/^([0-9]{4}){$delim}([0-9]{2}){$delim}([0-9]{2})$/");
	}

	/**
	 * start,end日付の妥当性をチェック
	 */
	public function end_large($start)
	{
		$end = $this->_CI->input->get('end');
		if(strtotime($start) > strtotime($end)){
			return false;
		}
		return true;
	}

	/**
	 * inを生成
	 */
	public function in($str, $key)
	{
		$list = $this->_CI->config->item('xconfig')[$key];
		$list = implode(',', array_keys($list));
		$this->set_message('in', $this->get_error_message('in_list', null, $list));
		return $this->in_list($str, $list);
	}
}
