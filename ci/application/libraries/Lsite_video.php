<?php

class Lsite_video
{
  private $_CI;

  public $status = [
    ['id' => 0, 'name' => 'disabled'], // 無効
    ['id' => 1, 'name' => 'enabled'], // 有効
  ];

  public function __construct()
  {
    $this->_CI =& get_instance();
    $this->_CI->load->library('lvideo');
  }

  public function status_id($name)
  {
    return items_find($this->status, 'name', $name)['id'];
  }

  public function status_name($id)
  {
    return items_find($this->status, 'id', $id)['name'];
  }

  public function get_videos($params, $page = 1, $limit = PRE_PAGE)
  {
    $site_seq = selection($params, 'site_seq');
    $tag_name = selection($params, 'tag_name');
    $done = selection($params, 'done', 1);
    $status = selection($params, 'status', 'enabled');
    $sort = selection($params, 'sort', 'site_videos.video_seq desc');
    $selects = selection($params, 'selects', 'site_videos.*, GROUP_CONCAT(tags.name) as tags');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('site_videos');
    $this->_CI->db->join('video_tag_map', 'video_tag_map.video_seq = site_videos.video_seq', 'left');
    $this->_CI->db->join('tags', 'tags.seq = video_tag_map.tag_seq', 'left');
    if ($site_seq) $this->_CI->db->where('site_videos.site_seq', $site_seq);
    if ($tag_name) $this->_CI->db->where('tags.name', $tag_name);
    if ($status) $this->_CI->db->where('status', $this->status_id($status));
    if ($done) $this->_CI->db->where('done', $done);
    $this->_CI->db->group_by('site_videos.video_seq');
    $tmpdb = clone $this->_CI->db;
    $total = $tmpdb->count_all_results();
    $tmpdb = null;

    $this->_CI->db->order_by($sort);
    $this->_CI->db->limit($limit, get_offset($limit, $page));
    $result = array_optimize($this->_CI->db->get()->result_array());
    // print_r($this->_CI->db->last_query());exit;
    // write_log("debug", $this->_CI->db->last_query());
    $result = array_map(function ($item) {
      if (isset($item['status'])) $item['status'] = $this->status_name($item['status']);
      if (isset($item['tags'])) $item['tags'] = ($item['tags'])? explode(',', $item['tags']) : [];
      return $item;
    }, $result);
    return [$total, $result];
  }

  public function get_video($params)
  {
    $video_seq = selection($params, 'video_seq');
    $video_id = selection($params, 'video_id');
    $status = selection($params, 'status');
    $selects = selection($params, 'selects', 'site_videos.*, GROUP_CONCAT(tags.name) as tags');

    $this->_CI->db->select($selects);
    $this->_CI->db->from('site_videos');
    $this->_CI->db->join('video_tag_map', 'video_tag_map.video_seq = site_videos.video_seq', 'left');
    $this->_CI->db->join('tags', 'tags.seq = video_tag_map.tag_seq', 'left');
    if ($video_seq) $this->_CI->db->where('site_videos.seq', $video_seq);
    if ($video_id) $this->_CI->db->where('site_videos.id', $video_id);
    if ($status) $this->_CI->db->where('site_videos.status', $this->status_id($status));
    $result = array_optimize($this->_CI->db->get()->row_array());
    if ($result) {
      if (isset($result['tags'])) $result['tags'] = ($result['tags'])? explode(',', $result['tags']) : [];
      if (isset($result['status'])) $result['status'] = $this->status_name($result['status']);
    }
    return $result;
  }

  public function sync($site_seq)
  {
    list($total, $videos) = $this->_CI->lvideo->get_videos([], 1, 99999);
    $video_seqs = array_column($videos, 'seq');
    $site_video_seqs = $this->get_have_video_seqs($video_seqs);
    $create_list = $update_list = [];
    foreach ($videos as $key => $item) {
      $item['site_seq'] = $site_seq;
      $item['video_seq'] = $item['seq'];
      unset($item['seq'], $item['created_at'], $item['updated_at']);
      if (in_array($item['video_seq'], $site_video_seqs)) {
        $update_list[] = $item;
        continue;
      }
      $create_list[] = $item;
    }
    if ($create_list) $this->_CI->db->insert_batch('site_videos', $create_list);

    foreach ($update_list as $key => $item) {
      $this->update_sync($item);
    }
  }

  public function delete($site_seq)
  {
    $this->_CI->db->delete('site_videos', ['site_seq' => $site_seq]);
  }

  public function get_have_video_seqs($video_seqs)
  {
    $this->_CI->db->select('video_seq');
    $this->_CI->db->from('site_videos');
    $this->_CI->db->where_in('video_seq', $video_seqs);
    $result = array_optimize($this->_CI->db->get()->result_array());
    return array_column($result, 'video_seq');
  }

  public function update($params)
  {
    $site_seq = $params['site_seq'];
    $video_seq = $params['video_seq'];
    $data = [
      'title' => $params['title'],
      'description' => $params['description'],
      'keywords' => $params['keywords'],
      'comment' => $params['comment'],
      'play_count' => $params['play_count'],
      'good_count' => $params['good_count'],
      'bad_count' => $params['bad_count'],
      'done' => $params['done']
    ];
    $res = $this->_CI->db->update('site_videos', $data, ['site_seq' => $site_seq, 'video_seq' => $video_seq]);
    return $res;
  }

  public function update_sync($params)
  {
    $site_seq = $params['site_seq'];
    $video_seq = $params['video_seq'];
    $data = [
      'url' => $params['url'],
      'eyecatch' => $params['eyecatch'],
      'status' => $params['status']
    ];
    $res = $this->_CI->db->update('site_videos', $data, ['site_seq' => $site_seq, 'video_seq' => $video_seq]);
    return $res;
  }
}
