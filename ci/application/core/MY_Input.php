<?php

class MY_Input extends CI_Input {

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * GETとPOSTである方の値を全て取得
   *
   * @param [type] $xss_clean
   * @return void
   */
  public function request($xss_clean = null)
  {
    $request = $this->get(null, $xss_clean);
    if(!$request) $request = $this->post(null, $xss_clean);
    $request = array_optimize($request);
    return $request;
  }
}
