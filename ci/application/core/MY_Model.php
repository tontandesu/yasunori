<?php

class MY_Model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  // TODO 元を拡張する
  public function result_array() {
    $result = $this->db->get()->result_array();
    array_optimize($result);
    return $result ?: [];
  }
}
