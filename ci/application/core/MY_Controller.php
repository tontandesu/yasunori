<?php

class MY_Controller extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
  }
}

class Web_Controller extends MY_Controller {

  protected $data = [
    'service_name' => '', // サービス名
    'service_name_kana' => '',
    'title' => '', // 各ページのタイトル
    'sub_title' => '', // パイプの後ろに付くページタイトル
    'keywords' => '',
    'description' => '',
    'eyecatch' => '',
    'apple_touch_icon' => '',
    'noindex' => false,
    'noreferrer' => false,
    'assets_id' => '',
    'rss_title' => '新着動画',
    'rss_url' => '',
    'is_h1' => false, // ヘッダー内H1を有効にする
    'is_eyecatch' => false, // アイキャッチを有効にする
    'free_header' => '',
    'free_footer' => '',
  ];

  protected $admin_hosts = [
    'yasu-admin.test',
    '52.68.194.249'
  ];

  public function __construct()
  {
    parent::__construct();
    $this->data['apple_touch_icon'] = get_http_url() . '/img/app.png';
    $this->data['eyecatch'] = get_http_url() . '/img/eyecatch.svg';
    $domain = $_SERVER['HTTP_HOST'];

    // ログイン・ログアウト
    if (in_array($this->router->method, ['login', 'logout'])) {
      return;
    }

    // ADMIN実行
    if (in_array($domain, $this->admin_hosts)) {
      $this->load->library('luser');
      $this->luser->is_login(['admin']);
      $this->data['assets_id'] = 'admin-index';
      echo $this->load->view('admin/index', $this->data, true);
      exit;
    }

    // SITE実行
    // ドメインからサイト設定を取得してい表示する title, disc, theme...
    // $this->data['apple_touch_icon'] = get_http_url() . '/img/app.png';
    // $this->data['eyecatch'] = get_http_url() . '/img/eyecatch.jpg';
    // $this->data['rss_url'] = get_http_url() . '/feed';
    $this->load->library('lsite');
    $site = $this->lsite->get_site(['domain' => $domain]);
    if (!$site) exit;
    $this->data['service_name'] = $site['name'];
    $this->data['service_name_kana'] = $site['kana'];
    $this->data['sub_title'] = $site['copy'];
    $this->data['keywords'] = $site['keywords'];
    $this->data['description'] = $site['description'];
    if ($site['eyecatch']) $this->data['eyecatch'] = get_http_url() . $site['eyecatch'];
    $this->data['assets_id'] = $site['theme'] . '-site';
    $this->data['free_footer'] = $site['script'];

    $this->load->library('ltag');
    $tags = $this->ltag->get_video_tags(['site_seq' => $site['seq']]);
    // $this->data['tags'] = array_column($tags, 'name');
    $this->data['tags'] = [];
    $this->data['recommend_tags'] = [];
    foreach ($tags as $key => $tag) {
      $this->data['tags'][] = $tag['name'];
      if ($tag['recommend']) $this->data['recommend_tags'][] = $tag['name'];
    }
  }

  public function get_home_title()
  {
    $t1 = $this->data['service_name'];
    $t2 = $this->data['service_name_kana'] ? "（{$this->data['service_name_kana']}）" : '';
    $t3 = ($this->data['sub_title'])? " | {$this->data['sub_title']}" : '';
    return "{$t1}{$t2}{$t3}";
  }

  public function get_page_title($title)
  {
    $t2 = ($this->data['service_name'])? " - {$this->data['service_name']}" : '';
    $t3 = $this->data['service_name_kana'] ? "（{$this->data['service_name_kana']}）" : '';
    return "{$title}{$t2}{$t3}";
  }

  public function show_404()
  {
    $this->data['assets_id'] = 'site-index';
    $this->data['is_h1'] = true;
    $this->output->set_status_header('404');
    $this->data['title'] = $this->get_page_title('404');
    $this->load->view('error/404', $this->data);
  }
}

class Api_Controller extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
  }
}
