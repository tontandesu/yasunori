<?php

function inp_text($name, $default=null, $option="") {
    return form_input($name, set_value($name, $default), $option);
}

function inp_textarea($name, $default=false, $option="") {
    return form_textarea($name, r(set_value($name, $default)), $option);
}

function inp_hidden($name, $default=false, $option="") {
    return form_hidden($name, set_value($name, $default), $option);
}

function inp_password($name, $default=false, $option="") {
    return form_password($name, set_value($name, $default), $option);
}

function inp_dropdown($name, $list, $default=null, $option="") {
    return form_dropdown($name, $list, set_value($name, $default), $option);
}
function inp_multiselect($name, $list, $default=[], $option="") {
    return form_multiselect($name, $list, set_value($name, $default), $option);
}

function inp_checkboxs($name, $list, $default=[], $section="<br />") {
    $html = "";
    $cnt = 1;
    foreach($list as $id => $val){
        $op = false;
        if ($default !== null && in_array($id, $default)) $op = true;
        $label_id = $name.$cnt;
        $html .= form_checkbox("{$name}[]", $id, set_checkbox("{$name}[]", $id, $op), "id='{$label_id}'") ."<label for='{$label_id}'>{$val}</label>{$section}";
        $cnt++;
    }
    return $html;
}

function inp_radios($name, $list, $default=null) {
    $html = "";
    $cnt = 1;
    foreach($list as $id => $val){
        $op = false;
        if ($id == $default) $op = true;
        $label_id = $name.$cnt;
        $html .= form_radio($name, $id, set_radio($name, $id, $op), "id='{$label_id}'") ."<label for='{$label_id}' class='right10'>{$val}</label>";
        $cnt++;
    }
    return $html;
}

function inp_user_level($name, $default=null, $option="") {
    return form_dropdown($name, get_xconfig("user_level"), set_value($name, $default), $option);
}

function inp_permissions($name, $default=null, $option="") {
    $list = get_permissions();
    unset($list[1]);
    // $list = array_values($permissions);
    return form_dropdown($name, $list, set_value($name, $default), $option);
}

function inp_gender($name, $default=null, $option="") {
    return form_dropdown($name, get_genders(), set_value($name, $default), $option);
}
