<?php

function items_find($items, $key, $val) {
  foreach ($items as $item) {
    // integerやfloatでも型が違ってくるのでここでは、== を使う
    if ($item[$key] == $val) {
      return $item;
    }
  }
  return null;
}

function get_datetime($option = 'now', $target_date = null, $format = 'Y-m-d H:i:s') {
  if (!$target_date) $target_date = time();
  return date($format, strtotime($option, $target_date));
}







/**
 * offset値を取得
 */
function get_offset($limit, $page=1){
    return $limit * ($page - 1);
}

/**
 * 配列からキーを削除してindexを詰める
 *
 * @param array $arr
 * @param string $key
 * @return array
 */
function remove_array_key(&$arr, $key){
    unset($arr[$key]);
    $arr = array_values($arr);
}

function get_token()
{
    return md5(uniqid(rand()));
}

/**
 * http urlを取得
 */

 /**
  * 現在のベースURLを返す
  * ELBを使うのでHTTPS固定とする
  *
  * @return string
  */
function get_http_url()
{
    if (isset($_SERVER["HTTP_HOST"])) {
        $http = (ENVIRONMENT == "development")? "http" : "https";
        return "{$http}://{$_SERVER["HTTP_HOST"]}";
    }
    return null;
}

function get_referer()
{
    if (isset($_SERVER["HTTP_REFERER"])) {
        return $_SERVER["HTTP_REFERER"];
    }
    return "";
}

/**
 * スネークケースをキャメルケースに変換する
 * @param string $str
 * @param string $except
 */
function change_camelize($str, $except=null){
    if($except) $str = str_replace($except, "", $str);
    return lcfirst(strtr(ucwords(strtr($str, ['_' => ' '])), [' ' => '']));
}

/**
 * キャメルケースをスネークケースに変換する
 * @param string $str
 * @param string $except
 */
function change_underscore($str, $except=null){
    if($except) $str = str_replace($except, "", $str);
    return ltrim(strtolower(preg_replace('/[A-Z]/', '_\0', $str)), '_');
}

/**
 * 日付を指定の形式にフォーマットする
 * @param string $date
 * @param string $format
 */
function _date_format($date, $format="Y/m/d H:i"){
    return date_format(date_create($date), $format);
}

/**
 * cache_idを生成する
 * @param array $array idの元になる配列
 */
function get_cache_id($array){
    $id = "";
    foreach($array as $val){
        if(is_array($val)){
            $id .= json_encode($val);
        }else{
            $id .= $val;
        }
    }
    return md5($id);
}

/**
 * 配列内から指定の値を取り出す
 * @param array $array
 * @param string $item
 * @param string $default
 */
function selection($array, $item, $default=null){
    return array_key_exists($item, $array)? $array[$item] : $default;
}

/**
 * 配列内の特定の値を削除する
 * @param array $array 元配列
 * @param array $delete_keys 削除したい値の配列
 */
function delete_array($array, $delete_keys=[]){
    $result = array_diff($array, $delete_keys);
    return array_values($result);
}

/**
 * マスター配列から値を抜き出す
 * @param array $master_array 元配列
 * @param array $select_array 抜き出したいidの配列
 */
function select_master_array($master_array, $select_array){
    $result = [];
    foreach($select_array as $key){
        if(in_array($key, $master_array)){
            $result[] = $key;
        }
    }
    return $result;
}

/**
 * 配列内の特定キーの後に値を追加する
 * @param array $array 元配列
 * @param string $target_key 検索する値
 * @param string $add_key 追加する値
 */
function add_after_array($array, $target_key, $add_key){
    if($key = array_search($target_key, $array) !== false){
        array_splice($array, ($key + 1), 0, $add_key);
    }
    return $array;
}

/**
 * 配列内の特定キーの前に値を追加する
 * @param array $array 元配列
 * @param string $target_key 検索する値
 * @param string $add_key 追加する値
 */
function add_before_array($array, $target_key, $add_key){
    $key = array_search($target_key, $array);
    if($key !== false){
        array_splice($array, $key, 0, $add_key);
    }
    return $array;
}

/**
 * 検索文字列の次の値を取得する
 *
 * @param [type] $array
 * @param [type] $str
 * @return void
 */
function get_next_array_value($array, $target_val){
    $result = null;
    $key = array_search($target_val, $array);
    if ($key !== false) $result = selection($array, $key+1);
    return $result;
}

/**
 * リスト配列の特定のキーで対象の値が含まれる値を取得する
 *
 * @param [type] $list
 * @param [type] $target_key
 * @param [type] $target_val
 * @return void
 */
function get_list_search_values($list, $target_key, $target_val){
    $result = [];
    foreach ($list as $val) {
        if ($val[$target_key] == $target_val) {
            $result[] = $val;
        }
    }
    return $result;
}

/**
 * 配列をページングする
 * @param array $list 元配列
 * @param integer $page
 */
function pagination_array(&$array, $page=1){
    // スタート番号取得
    $start = 0;
    if($page > 1){
        $start = PRE_PAGE * ($page -1);
    }
    return array_slice($array, $start, PRE_PAGE);
}

/**
 * ログ出力
 * @param string $level record, error, debug, info, all
 * @param string $message 出力メッセージ
 * @param string $log_id ログファイルのID
 */
function write_log($level, $message, $log_id="log-"){
    static $_log;
    if($_log === NULL) $_log[0] =& load_class('Log', 'core');
    $_log[0]->write_log($level, $message, $log_id);
}

/**
 * DBからの戻り値をKV型に変換する
 *
 * @param [type] $list
 * @param [type] $id_name
 * @param [type] $val_name
 * @return void
 */
function convert_to_kv($list, $id_name, $val_name){
    $result = [];
    foreach ($list as $val) {
        $result[$val[$id_name]] = $val[$val_name];
    }
    return $result;
}

/**
 * 選択肢に空配列を追加する
 *
 * @param [type] $kv
 * @return void
 */
function add_kv_empty($kv){
    return ["" => "---"] + $kv;
}

/**
 * 時間範囲の配列を取得する（0-23）
 */
function get_range_time_array(){
    return range(0, 23);
}

/**
 * 日付範囲の配列を取得する
 * @param string $start 開始日付 2016-07-01
 * @param string $end 終了日付 2016-07-20
 */
function get_range_date_array($start, $end){
    $result = [];
    $diff = (strtotime($end) - strtotime($start)) / ( 60 * 60 * 24);
    for($i = 0; $i <= $diff; $i++) {
        $result[] = date(DATE_FORMAT, strtotime($start . '+' . $i . 'days'));
    }
    return $result;
}

/**
 * 月範囲の配列を取得する
 * @param string $start 開始日付 2016-07-01
 * @param string $end 終了日付 2016-07-20
 */
function get_range_month_array($start, $end){
    $start = strtotime($start);
    $end = strtotime($end);

    $result = [];
    $tmp = $end;
    while($tmp >= $start){
        $result[] = date(DATE_FORMAT_MONTH, $tmp);
        $tmp = strtotime("-1 month", $tmp);
    }
    return $result;
}

/**
 * 配列内の値型を最適化する
 * @param array $array
 */
function array_optimize($array){
    if (!$array) return [];
    array_walk_recursive($array, function(&$value, $key){
        // 数値に変換
        if(is_numeric($value)){
            $value = floatval($value);
        }
    });
    return $array;
}

/**
 * queryStringから値を抜出す
 * @param string $querystring
 * @param string $key パラメータID
 */
function get_querystring_value($querystring, $key){
    $result = "";
    parse_str($querystring, $array);
    return selection($array, $key);
}

/**
 * 引き算
 * @param integer $val1
 * @param integer $val2
 */
function calc_subtraction($val1, $val2){
    return floatval($val1) - floatval($val2);
}

/**
 * 割り算
 * @param integer $val1
 * @param integer $val2
 */
function calc_division($val1, $val2){
    return $val2 == 0 ? 0 : floatval($val1) / floatval($val2);
}

/**
 * エスケープ
 * @param array|string $var 対象値
 * @param array $except 除外するキー名
 */
function h($var, $except=[]){
    if (is_array($var)){
        foreach($except as $key){
            if(isset($var[$key])) return $var;
        }
        return array_map('h', $var);
    }else{
        return htmlspecialchars($var, ENT_QUOTES, config_item('charset'));
    }
}

/**
 * エスケープ デコード
 * @param array|string $var 対象値
 * @param array $except 除外するキー名
 */
function r($var, $except=[]){
    if (is_array($var)){
        foreach($except as $key){
            if(isset($var[$key])) return $var;
        }
        return array_map('r', $var);
    }else{
        return htmlspecialchars_decode($var, ENT_QUOTES);
    }
}
