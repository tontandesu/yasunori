<?php

function get_limited_str($status) {
  return $status === 'limited' ? '[限定公開]' : '';
}

function view_lazy_img($src, $alt = '', $options = 'class="_lazy"')
{
  return <<<HTML
<img src="{NO_IMAGE_URL}" data-src="{$src}" alt="{$alt}" {$options}>
HTML;
}

function get_pagination($base_url, $total_rows, $pre_page){
    $CI =& get_instance();
    $CI->load->library("pagination");

    $config['base_url'] = $base_url;
    $config['total_rows'] = $total_rows;
    $config['per_page'] = $pre_page;
    $config['use_page_numbers'] = true;
    $config['num_links'] = 3;
    $config['uri_segment'] = substr_count($base_url, '/');
    $config['full_tag_open'] = '<ul class="xpagination">';
    $config['full_tag_close'] = '</ul>';
    $config['cur_tag_open'] = '<li class="xcurrent">';
    $config['cur_tag_close'] = '</li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    //$config['anchor_class'] = 'class="pagination" ';
    //$config['page_query_string'] = true;
    //$config['query_string_segment'] = "page";
    $config['next_link'] = '&gt;';
    $config['first_link'] = '&laquo;';
    $config['prev_link'] = '&lt;';
    $config['last_link'] = '&raquo;';

    $CI->pagination->initialize($config);
    return $CI->pagination->create_links();
}






/**
 * 開発環境チェック
 */
function is_dev() {
    return (ENVIRONMENT == "development");
}

/**
 * 開発環境チェック
 */
function is_mydomain($url) {
    if(is_dev()) return true;
    if (strpos($url, "http") !== 0) return true;

    $mydomain = MYDOMAIN;
    if (preg_match("/^https?:\/{2}(.+\.)?{$mydomain}/", $url)) {
        return true;
    }
    return false;
}

function get_xconfig($key)
{
    $CI =& get_instance();
    $xconfig = $CI->config->item("xconfig");
    return $xconfig[$key];
}

function get_password_rule($options) {
    return "trim|min_length[6]|max_length[20]|regex_match[/\A(?=.*?[a-z])(?=.*?\d)[a-z\d]{6,20}+\z/i]{$options}";
}

function get_action_status_list()
{
    return [
      "accepting" => "受付中",
      "solution" => "解決済み",
    //   "attention" => "注目",
    ];
}

function get_action_status($status)
{
  $list = get_action_status_list();
  return $list[$status];
}

function get_user_level()
{
    return [
        0 => "なし",
        1 => "C1",
        2 => "C2",
        3 => "C3",
        4 => "C4",
        5 => "C5",
    ];
}

function get_user_status()
{
    return [
        1 => "有効",
        0 => "無効",
    ];
}

function get_genders()
{
    // TODO 多言語化
    return [
        1 => "男",
        2 => "女",
    ];
}

function get_displays()
{
    return [
        0 => "表示しない",
        1 => "表示する",
    ];
}

function get_enables()
{
    return [
        0 => "無効",
        1 => "有効",
    ];
}

function get_mail_type()
{
    return [
        "html" => "HTML",
        "text" => "TEXT",
    ];
}

function get_noindexs()
{
    return [
        0 => "しない",
        1 => "する",
    ];
}

function get_weeks()
{
    return ["日", "月", "火", "水", "木", "金", "土"];
}

function get_send_start_timing()
{
    return [
        0 => "登録直後",
        1 => "1日後",
        2 => "2日後",
        3 => "3日後",
        4 => "4日後",
        5 => "5日後",
    ];
}

function get_send_times()
{
    $result = [];
    for ($i=0; $i<=23; $i++) {
        $time = sprintf("%02d", $i);
        $result["{$time}:00"] = "{$time}:00";
        $result["{$time}:30"] = "{$time}:30";
    }
    return $result;
}

function get_send_time()
{
    $hh = intval(date("H"));
    $ii = intval(date("i"));

    // 時間の調整
    if ($ii > 50) {
        $hh++;
        if ($hh > 23) $hh = 0;
    }

    // 分の調整
    if ($ii > 50 || $ii < 10) {
        $ii = 0;
    } else if ($ii > 20 && $ii < 40) {
        $ii = 30;
    } else {
        return "";
    }

    $hh = sprintf("%02d", $hh);
    $ii = sprintf("%02d", $ii);

    return "{$hh}:$ii";
}

/**
 * HTMLタイトルタグの値を取得
 */
function get_title_tag_value($title, $site_title = "")
{
    if ($site_title) $title .= " | " . $site_title;
    return $title;
}

/**
 * アイキャッチURLを取得
 * @param string $name ファイル名
 */
function get_eyecatch_url($name = "ogimg-mylife.jpg"){
    return get_http_url() . "/img/eyecatch/{$name}";
}

function get_user_img_path($user_seq, $filename = null) {
    return "public/u{$user_seq}/{$filename}";
}

/**
 * 画像URLを取得
 * @param  [type] $user_seq [description]
 * @param  [type] $filename [description]
 */
function get_user_img_url($user_seq, $filename=null){
    return URL_S3 . "/public/u{$user_seq}/{$filename}";
}

/**
 * 辞書を読み込む
 * @param string $path assets配下の辞書ディレクトリまでのpath
 */
function load_lang_file($path){
    $CI =& get_instance();

    // 辞書を1日キャッシュする
    $array = get_cache(["id" => $path], function() use ($path){
        $lang_id = LANG_ID;
        $file_path = PATH_PJ . "assets/{$path}/translate/{$lang_id}.json";
        if(!file_exists($file_path)){
            show_error("No language file.", 500);
        }
        return json_decode(file_get_contents($file_path), true);
    }, !is_dev(), 86400);

    $CI->is_loaded[$path] = $path;
    $CI->language = LANG_ID;
    $CI->lang->language = array_merge($CI->lang->language, $array);
}

/**
 * json出力
 * @param array $array 出力する元配列
 * @param integer $status ステータスコード
 */
function render_json($array=[], $status=200){
    $CI =& get_instance();

    // add validation
    if(isset($CI->form_validation)){
        $errors = $CI->form_validation->error_array();
        if(count($errors)){
            // if(!is_dev()) $errors = [];
            $array = $errors;
            $status = 400;
        }
    }

    // JSON_NUMERIC_CHECK を指定するとエラーになるパターンがある
    // idでエラーになる場合がある
    $json_data = json_encode($array);
    // echo json_last_error_msg();
    // print_r($array);
    // print_r($json_data);
    // print_r($status);
    // exit;

    $output = $CI->output
        ->set_status_header($status)
        ->set_content_type('application/json')
        ->set_output($json_data)
        ->get_output();
    $CI->output->_display($output);
    exit;
    // fastcgi_finish_request();
}

function to_camelcase_array($array) {
    $results = [];
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $results[camelize($key)] = to_camelcase($value);
        } else {
            $results[camelize($key)] = $value;
        }
    }
    return $results;
}

/**
 * 言語を取得する
 * @param string $id 言語ID
 */
function get_lang($id, $replaces=[]){
    $CI =& get_instance();
    $str = $CI->lang->line($id);
    if($replaces) $str = vsprintf($str, $replaces);
    return $str;
}

/**
 * キャッシュを取得、なければ処理を実行して新規保存
 * @param array $cache_ids キャッシュIDに使用する配列
 * @param function $callback キャッシュがなかった場合の処理
 * @param boolean $use_cache キャッシュを使用するフラグ
 * @param boolean $cache_second キャッシュする秒数
 */
function get_cache($cache_ids, $callback, $use_cache=true, $cache_second=null){
    // 開発環境はキャッシュしない
    if(is_dev()) $use_cache = false;

    // init
    if(!$cache_second) $cache_second = CACHE_SECOND;

    // キャッシュを使う
    $CI =& get_instance();
    $cache_id = get_cache_id($cache_ids);
    if($cache_data = $CI->cache->get($cache_id)){
        return $cache_data;
    }
    $result = $callback();
    $CI->cache->save($cache_id, $result, $cache_second);
    return $result;
}

/**
 * view cacheを出力
 */
function output_view_cache(){
    $CI =& get_instance();
    if(!is_dev()){
        $CI->output->cache(CACHE_SECOND);
    }
}

/**
 * バージョンパラメータを追加
 */
function dvn(){
    $result = "";
    //if(is_dev()){
        $result = "v=".time();
    //}
    return $result;
}

/**
 * rootユーザー
 */
function is_root(){
    $CI =& get_instance();
    $user = $CI->session->userdata("user");
    if(!$user) return false;
    return ($user["permission"] == 1);
}

/**
 * adminユーザー
 */
function is_admin(){
    $CI =& get_instance();
    $user = $CI->session->userdata("user");
    if(!$user) return false;
    return ($user["permission"] <= 2);
}

/**
 * 全キャッシュ削除
 */
function delete_cache(){
    exec("rm -fr " . PATH_CACHE . "*");
}

function userlinkname($user_seq){
    return "_userlink{$user_seq}";
}

function create_good_key($question_seq, $answer_seq = null, $thread_seq = null) {
    return implode('_', [$question_seq, $answer_seq, $thread_seq]);
}

function get_user_name($user)
{
    return $user["nickname"]? $user["nickname"] : $user["user_id"];
}

function to_lazy_image_tag($html)
{
  $pattern = '/<(img.+?)src\s*=\s*[\"|\'](.*?)[\"|\'](.*?)>/i';
  return preg_replace($pattern, '<$1 src="'. NO_IMAGE_STR .'" data-src="$2" $3 class="_lazy">', $html);
}

