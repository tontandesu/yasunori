<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'site';
$route['translate_uri_dashes'] = true; // trueにするとハイフン型URLが使えるようになる
$route['404_override'] = 'site/_404';
$route['about']['get'] = 'site/about';
$route['privacy-policy']['get'] = 'site/privacy_policy';
$route['sitemap']['get'] = 'site/sitemap';
$route['feed']['get'] = 'site/feed';
$route['videos/tag/(.+)/(.+)']['get'] = 'site/videos_tag/$1/$2';
$route['videos/tag/(.+)']['get'] = 'site/videos_tag/$1';
$route['videos/(.+)']['get'] = 'site/videos/$1';
$route['videos']['get'] = 'site/videos';
$route['video/(.+)']['get'] = 'site/video/$1';

$route['login'] = 'user/login';
$route['logout']['get'] = 'user/logout';

$route['api/user']['get'] = 'api/user/get';

$route['api/sites']['get'] = 'api/site/search';
$route['api/sites/(.+)']['get'] = 'api/site/search/$1';
$route['api/site']['post'] = 'api/site/save';
$route['api/site/videos']['get'] = 'api/site/videos_search';
$route['api/site/videos/(.+)']['get'] = 'api/site/videos_search/$1';
$route['api/site/videos-sync']['post'] = 'api/site/videos_sync';
$route['api/site/delete-videos']['post'] = 'api/site/delete_videos';
$route['api/site/video']['post'] = 'api/site/video_save';

$route['api/site/(.+)']['get'] = 'api/site/get/$1';
$route['api/site/(.+)']['delete'] = 'api/site/delete/$1';

$route['api/themes']['get'] = 'api/site/themes';
$route['api/tags']['get'] = 'api/tag/search';
$route['api/tag']['post'] = 'api/tag/save';
$route['api/tag/(.+)']['delete'] = 'api/tag/delete/$1';

$route['api/videos']['get'] = 'api/video/search';
$route['api/video']['post'] = 'api/video/save';
$route['api/video/(.+)']['get'] = 'api/video/get/$1';
$route['api/video/(.+)']['delete'] = 'api/video/delete/$1';

$route['api/content/image']['post'] = 'api/content/image_upload';

$route['(.+)']['get'] = 'site/index/$1';


// $route['register']['get'] = 'user/register';
// $route['register/(.+)']['get'] = 'user/register';
// $route['profile/(.+)']['get'] = 'user/profile/$1';

// $route['question/input']['get'] = 'question/input';
// $route['question/list']['get'] = 'question/list';
// $route['question/list/(.+)']['get'] = 'question/list/$1';
// $route['question/(.+)']['get'] = 'question/index/$1';

// $route['questions/tag/(.+)']['get'] = 'question/tag/$1';
// $route['questions/tag/(.+)/(.+)']['get'] = 'question/tag/$1/$2';
// $route['questions']['get'] = 'question/search';
// $route['questions/(.+)']['get'] = 'question/search/$1';

// $route['password/send']['get'] = 'user/password_reset';
// $route['password/sent']['get'] = 'user/password_reset';
// $route['password/input/(.+)']['get'] = 'user/password_reset/$1';

// // 会員まわり
// $route['email-auth/conf/(.+)']['get'] = 'user/email_auth/$1';
// $route['email-reset/conf/(.+)']['get'] = 'user/email_reset/$1';
// $route['api/email-reset/send']['post'] = 'api/user/send_email_reset';
// $route['api/password']['post'] = 'api/user/update_password';

// // コラムまわり
// $route['column/(.+)']['get'] = 'column/index/$1';

// $route['settings']['get'] = 'settings/index';
// $route['settings/(.+)']['get'] = 'settings/index';

// $route['api/user']['get'] = 'api/user/get';
// $route['api/user']['post'] = 'api/user/register';
// $route['api/user/min']['get'] = 'api/user/min';
// $route['api/user/(.+)']['post'] = 'api/user/update/$1';
// $route['api/question']['get'] = 'api/question/get';
// $route['api/question']['post'] = 'api/question/save';
// $route['api/question/init']['get'] = 'api/question/init';
// $route['api/question/(.+)']['delete'] = 'api/question/delete/$1';
// $route['api/question/tags']['get'] = 'api/question/get_question_tags';
// $route['api/answer']['post'] = 'api/question/save_answer';
// $route['api/answer/(.+)']['delete'] = 'api/question/delete_answer/$1';
// $route['api/thread']['post'] = 'api/question/save_thread';
// $route['api/thread/(.+)']['delete'] = 'api/question/delete_thread/$1';
// $route['api/bestanswer/(.+)']['post'] = 'api/question/create_bestanswer/$1';
// $route['api/bestanswer/(.+)']['delete'] = 'api/question/delete_bestanswer/$1';
// $route['api/good']['post'] = 'api/question/create_good';
// $route['api/good/delete']['post'] = 'api/question/delete_good';
// $route['api/image']['get'] = 'api/image/get';
// $route['api/notification']['get'] = 'api/notification/index';
// $route['api/notification']['post'] = 'api/notification/save';
// $route['api/contents/image']['post'] = 'api/contents/image_upload';

// // コラムの一覧は、フロントのコラム一覧を改良して代用する
// $route['columns']['get'] = 'column/search';
// $route['columns/(.+)']['get'] = 'column/search/$1';
// $route['api/columns']['get'] = 'api/column/search';
// $route['api/columns/(.+)']['get'] = 'api/column/search/$1';
// $route['api/column']['post'] = 'api/column/save';
// $route['api/column/(.+)']['get'] = 'api/column/get/$1';
// $route['api/column/(.+)']['delete'] = 'api/column/delete/$1';

// $route['api/password-reset/send']['post'] = 'api/user/password_send';
// $route['api/password-reset/input']['post'] = 'api/user/password_input';




