<?php

$config['xconfig'] = [
  "mailmaga" => [
    "changing_life" => null,
  ],

  "permissions" => [
    1 => "root",
    2 => "admin",
    3 => "writer",
  ],

  /**
   * 会員レベル
   * C1:0人以上, C2:5人以上, C3:30人以上, C4:60人以上, C5:100人以上
   */
  "user_level" => [
    1 => "C1",
    2 => "C2",
    3 => "C3",
    4 => "C4",
    5 => "C5",
  ],
];
