<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


// 自分のドメイン
define('MYDOMAIN', getenv('VIRTUAL_HOST'));
define('NO_VIDEO_IMAGE_URL', '/img/no-video-image.jpg');
define('NO_IMAGE_URL', '/img/no.svg');
define('NO_IMAGE_STR', 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=');
define('EMPTY_ACCOUNT_URL', '/img/empty-account.svg');

define('PATH_PJ', dirname(APPPATH).'/');
define('PATH_CACHE', APPPATH . "cache/");
define('PATH_LIB', APPPATH . "libraries/");
define('PATH_CONTENTS', FCPATH . "contents/");
define('PATH_PUBLIC', PATH_PJ . "public/");

define('EMAIL_NOTICE', 'notice@m-shovel.com'); // 管理者へのお知らせ用アドレス
define('ADMIN_EMAIL', 'info@m-shovel.com');
define('FROM_EMAIL', 'info@m-shovel.com');
define('FROM_EMAIL_NAME', 'シャベル運営事務局');
$email_footer = <<<TEXT
──────────
シャベル運営事務局
Home: https://m-shovel.com
Email: info@m-shovel.com
──────────
TEXT;
define('EMAIL_FOOTER', $email_footer);
define('AWS_ACCESS_KEY', isset($_SERVER['AWS_ACCESS_KEY_ID']) ? $_SERVER['AWS_ACCESS_KEY_ID'] : '');
define('AWS_SECRET_KEY', isset($_SERVER['AWS_SECRET_ACCESS_KEY']) ? $_SERVER['AWS_SECRET_ACCESS_KEY'] : '');

define('DATE_TIME_FORMAT', 'Y-m-d H:i:s');
define('DATE_FORMAT', 'Y-m-d');
define('MONTH_FORMAT', 'Y-m');

define('URL_S3', 'https://qamarketing.s3-ap-northeast-1.amazonaws.com');
define('URL_S3_PUBLIC', URL_S3 . '/public');

// キャッシュ有効期限(秒): 1時間
define('CACHE_SECOND', 3600);

// 1ページあたりに表示したい件数
define('PRE_PAGE', 24);

// 通知設定がない場合の最初の挙動 1:通知する, 0:通知しない
define('IS_INIT_NOTICE', 1);
