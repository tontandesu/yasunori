<?php

class Content extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->library('form_validation');
        $this->load->library('luser');
    }


    public function image_upload()
    {
        $result = [
            'filename' => null,
            'path' => null,
            'message' => null,
        ];

        if ($this->input->method() !== 'post' || !$_FILES) render_json($result, 404);
        if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

        // TODO 実態を参照した拡張子判定
        $extension = '';
        if ($_FILES['userfile']['type'] == 'image/jpeg') $extension = 'jpg';
        elseif ($_FILES['userfile']['type'] == 'image/png') $extension = 'png';
        elseif ($_FILES['userfile']['type'] == 'image/gif') $extension = 'gif';
        else $extension = '';
        if (!$extension) {
            render_json($result, 400);
        }

        $filename = md5(microtime(true)).".{$extension}";
        $urlpath = 'contents';
        $config['upload_path'] = PATH_PUBLIC.$urlpath;
        if (!file_exists($config['upload_path'])) mkdir($config['upload_path'], 0755);
        $config['allowed_types'] = '*';
        $config['max_size']    = '1024'; // 0.5M
        $config['file_name'] = $filename;
        $this->upload->initialize($config);
        if ($this->upload->do_upload()) {
            $upload_data = $this->upload->data();
            $result['filename'] = $upload_data['file_name'];
            $result['urlpath'] = '/'.$urlpath.'/'.$result['filename'];
            render_json($result);
        }else{
            $result['message'] = $this->upload->display_errors();
            render_json($result, 400);
        }
    }

    public function image_list()
    {
        $result = [];
        if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);
        $user = $this->luser->get_login_user();
        $urlpath = "contents/{$user['user_seq']}/";
        $dir_path = PATH_PUBLIC."{$urlpath}{*.gif,*.jpg,*.jpeg,*.png}";
        foreach(glob($dir_path, GLOB_BRACE) as $file) {
            if (is_file($file)) {
                $result[] = "/{$urlpath}".basename($file);
            }
        }

        render_json($result);
    }
}
