<?php

class Tag extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('luser');
    $this->load->library('ltag');
  }

  public function search()
  {
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    $result = $this->ltag->get_tags();
    render_json($result);
  }

  public function save()
  {
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    $result = [];
    $this->form_validation->set_rules('seq', null, 'trim|numeric');
    $this->form_validation->set_rules('name', 'タグ名', 'trim|required|callback__no_overlap_name');
    $this->form_validation->set_rules('kana', null, 'trim|required');
    $this->form_validation->set_rules('recommend', null, 'trim|required|numeric');
    if (!$this->form_validation->run()) {
      render_json($result, 400);
    }

    $params = $this->input->post();
    try {
      $tag_seq = $this->ltag->save_tag($params);
    } catch (Exception $e) {
      render_json($result, 500);
    }
    $result['seq'] = $tag_seq;
    render_json($result);
  }

  public function delete($tag_seq = null)
  {
    $result = [];
    if (!$tag_seq) render_json($result, 400);
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    try {
      $this->ltag->delete_tag($tag_seq);
    } catch (Exception $e) {
      render_json($result, 500);
    }
    render_json($result);
  }

  function _no_overlap_name($name) {
    if (!$name) return true;

    $tag_seq = $this->input->post('seq');
    if ($tag_seq) {
      $old_tag = $this->ltag->get_tag(['tag_seq' => $tag_seq]);
      if ($old_tag['name'] == $name) {
        return true;
      }
    }

    $tag = $this->ltag->get_tag(['name' => $name]);
    if ($tag) {
      $this->form_validation->set_message('_no_overlap_domain', '{field}は既に登録されています');
      return false;
    }
    return true;
  }
}
