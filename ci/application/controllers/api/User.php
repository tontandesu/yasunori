<?php

class User extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('encryption');
    $this->load->library('luser');
  }

  public function min()
  {
    $user = $this->luser->get_login_user();
    render_json([
      'user_seq' => selection($user, 'user_seq'),
      'user_id' => selection($user, 'user_id'),
      'display_name' => selection($user, 'display_name'),
      'icon' => selection($user, 'icon'),
    ]);
  }

  public function get()
  {
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    $user_seq = $this->luser->get_current_user_seq();
    $selects = 'seq,id,email,permission,nickname,icon,message';
    $result = $this->luser->get_user(['user_seq' => $user_seq, 'selects' => $selects]);
    render_json($result);
  }

  function _overlap_email($email)
  {
    if (!$email) return true;
    $this->db->select('user_seq');
    $user_data = $this->db->get_where('user', ['email' => $email])->result_array();
    if ($user_data) {
        $this->form_validation->set_message('_overlap_email', '{field}は既に登録されています');
        return false;
    }
    return true;
  }

  function _overlap_userid($user_id)
  {
    if (!$user_id) return true;
    $this->db->select('user_seq');
    $user_data = $this->db->get_where('user', ['user_id' => $user_id])->result_array();
    if ($user_data) {
        $this->form_validation->set_message('_overlap_userid', '{field}は既に登録されています');
        return false;
    }
    return true;
  }

}
