<?php

class Video extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('luser');
    $this->load->library('lvideo');
  }

  public function get($video_seq = null)
  {
    $result = [];
    if (!$video_seq) render_json($result, 400);

    $result = $this->lvideo->get_video(['video_seq' => $video_seq]);
    render_json($result);
  }

  public function search($page = 1)
  {
    $result = [];
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    // $params = $this->input->get();
    list($total, $result) = $this->lvideo->get_videos(['status' => ''], $page);
    render_json(['list' => $result, 'total' => $total]);
  }

  public function themes()
  {
    $result = [
      ['id' => 'sky', 'name' => 'スカイブルー'],
      ['id' => 'yellow', 'name' => 'イエロー'],
      ['id' => 'pink', 'name' => 'ピンク'],
    ];
    render_json($result);
  }

  public function save()
  {
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    $result = [];
    $this->form_validation->set_rules('seq', null, 'trim|numeric');
    $this->form_validation->set_rules('id', 'ID', 'trim|alpha_dash|callback__no_overlap_id');
    $this->form_validation->set_rules('title', null, 'trim|required');
    $this->form_validation->set_rules('description', null, 'trim');
    $this->form_validation->set_rules('keywords', null, 'trim');
    $this->form_validation->set_rules('url', 'URL', 'trim|callback__no_overlap_url');
    $this->form_validation->set_rules('eyecatch', null, 'trim');
    $this->form_validation->set_rules('comment', null, 'trim');
    $this->form_validation->set_rules('play_count', null, 'trim|numeric');
    $this->form_validation->set_rules('good_count', null, 'trim|numeric');
    $this->form_validation->set_rules('bad_count', null, 'trim|numeric');
    $this->form_validation->set_rules('status', null, 'trim|required|in_list[disabled,enabled]');
    $this->form_validation->set_rules('tags[]', null, 'trim');
    if (!$this->form_validation->run()) {
      render_json($result, 400);
    }

    $params = $this->input->post();
    try {
      $video_seq = $this->lvideo->save_video($params);
    } catch (Exception $e) {
      render_json($result, 500);
    }
    render_json($result);
  }

  public function delete($video_seq = null)
  {
    $result = [];
    if (!$video_seq) render_json($result, 400);
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    try {
      $video_seq = $this->lvideo->delete_video($video_seq);
    } catch (Exception $e) {
      render_json($result, 500);
    }
    render_json($result);
  }

  function _no_overlap_id($id) {
    if (!$id) return true;

    $seq = $this->input->post('seq');
    if ($seq) {
      $old = $this->lvideo->get_video_min(['video_seq' => $seq]);
      if ($old['id'] == $id) {
        return true;
      }
    }
    $video = $this->lvideo->get_video_min(['video_id' => $id]);
    if ($video) {
      $this->form_validation->set_message('_no_overlap_id', '{field}は既に登録されています');
      return false;
    }
    return true;
  }
  function _no_overlap_url($url) {
    if (!$url) return true;

    $seq = $this->input->post('seq');
    if ($seq) {
      $old = $this->lvideo->get_video_min(['video_seq' => $seq]);
      if ($old['url'] == $url) {
        return true;
      }
    }
    $video = $this->lvideo->get_video_min(['url' => $url]);
    if ($video) {
      $this->form_validation->set_message('_no_overlap_url', '{field}は既に登録されています');
      return false;
    }
    return true;
  }
}
