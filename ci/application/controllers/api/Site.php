<?php

class Site extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('luser');
    $this->load->library('lsite');
    $this->load->library('lsite_video');
  }

  public function get($site_seq = null)
  {
    $result = [];
    if (!$site_seq) render_json($result, 400);

    $result = $this->lsite->get_site(['site_seq' => $site_seq]);
    render_json($result);
  }

  public function search($page = 1)
  {
    $result = [];
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    $params = $this->input->get();
    list($total, $result) = $this->lsite->get_sites([], $page);
    render_json(['list' => $result, 'total' => $total]);
  }

  public function themes()
  {
    $result = [
      ['id' => 'sky', 'name' => 'スカイブルー'],
      ['id' => 'yellow', 'name' => 'イエロー'],
      ['id' => 'pink', 'name' => 'ピンク'],
    ];
    render_json($result);
  }

  public function save()
  {
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    $result = [];
    $this->form_validation->set_rules('seq', null, 'trim|numeric');
    $this->form_validation->set_rules('domain', 'ドメイン', 'trim|required|callback__no_overlap_domain');
    $this->form_validation->set_rules('name', null, 'trim|required');
    $this->form_validation->set_rules('kana', null, 'trim');
    $this->form_validation->set_rules('copy', null, 'trim|required');
    $this->form_validation->set_rules('description', null, 'trim');
    $this->form_validation->set_rules('keywords', null, 'trim');
    $this->form_validation->set_rules('eyecatch', null, 'trim');
    $this->form_validation->set_rules('theme', null, 'trim|required');
    $this->form_validation->set_rules('status', null, 'trim|required|in_list[disabled,enabled]');
    $this->form_validation->set_rules('script', null, 'trim');
    if (!$this->form_validation->run()) {
      render_json($result, 400);
    }

    $params = $this->input->post();
    try {
      $site_seq = $this->lsite->save_site($params);
    } catch (Exception $e) {
      render_json($result, 500);
    }
    render_json($result);
  }

  public function delete($site_seq = null)
  {
    $result = [];
    if (!$site_seq) render_json($result, 400);
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    try {
      $site_seq = $this->lsite->delete_site($site_seq);
    } catch (Exception $e) {
      render_json($result, 500);
    }
    render_json($result);
  }

  public function videos_search($page = 1)
  {
    $result = [];
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);
    // TODO: 検索
    $params = $this->input->get();
    $_params = ['done' => ''];
    if (!empty($params['s'])) $_params['site_seq'] = $params['s'];
    list($total, $result) = $this->lsite_video->get_videos($_params, $page, 99999);
    render_json(['list' => $result, 'total' => $total]);
  }

  public function videos_sync()
  {
    $result = [];
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    $site_seq = $this->input->post('seq');
    if (!$site_seq) render_json($result, 403);
    $this->lsite_video->sync($site_seq);
    render_json($result);
  }

  public function delete_videos()
  {
    $result = [];
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    $site_seq = $this->input->post('seq');
    if (!$site_seq) render_json($result, 403);
    $this->lsite_video->delete($site_seq);
    render_json($result);
  }

  public function video_save()
  {
    if (!$this->luser->is_login(['admin', 'editor'], false)) render_json($result, 403);

    $result = [];
    $this->form_validation->set_rules('site_seq', null, 'trim|required|numeric');
    $this->form_validation->set_rules('video_seq', null, 'trim|required|numeric');
    $this->form_validation->set_rules('title', null, 'trim');
    $this->form_validation->set_rules('description', null, 'trim');
    $this->form_validation->set_rules('keywords', null, 'trim');
    $this->form_validation->set_rules('comment', null, 'trim');
    $this->form_validation->set_rules('play_count', null, 'trim|numeric');
    $this->form_validation->set_rules('good_count', null, 'trim|numeric');
    $this->form_validation->set_rules('bad_count', null, 'trim|numeric');
    $this->form_validation->set_rules('done', null, 'trim|numeric');
    if (!$this->form_validation->run()) {
      render_json($result, 400);
    }

    $params = $this->input->post();
    try {
      $res = $this->lsite_video->update($params);
    } catch (Exception $e) {
      render_json($result, 500);
    }
    render_json($result);
  }

  function _no_overlap_domain($domain) {
    if (!$domain) return true;

    $site_seq = $this->input->post('seq');
    if ($site_seq) {
      $old = $this->lsite->get_site_min(['site_seq' => $site_seq]);
      if ($old['domain'] == $domain) {
        return true;
      }
    }
    $site = $this->lsite->get_site_min(['domain' => $domain]);
    if ($site) {
      $this->form_validation->set_message('_no_overlap_domain', '{field}は既に登録されています');
      return false;
    }
    return true;
  }
}
