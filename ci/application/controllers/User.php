<?php

class User extends Web_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('luser');
    $this->load->library('form_validation');
  }

  /**
   * 初回ユーザーをインストール
   * TODO シード的なやつに移す
   */
  public function install()
  {
    $values = [
      'user_id' => 'hitsuzikai',
      'email' => 'yamashitadade@gmail.com',
      'password' => 'wVP4gZ5b',
      'permission' => 'admin',
      'nickname' => '管理者',
      'icon' => '',
      'status' => 'enabled',
    ];
    $user = $this->luser->get_user(['user_id' => $values['user_id']], 'min');
    if ($user) {
      echo 'doen';
    } else {
      echo 'user_seq: ' . $this->luser->create($values);
    }
  }

  /**
   * ログイン
   *
   * @return void
   */
  public function login()
  {
    $this->data['show_error'] = false;
    if ($post = $this->input->post()) {
      $this->form_validation->set_rules('email', null, 'trim|required|valid_email');
      $this->form_validation->set_rules('password', null, 'trim|required');
      if ($this->form_validation->run()) {
        $is_login = false;
        $user_data = $this->luser->get_user([
          'email' => $post['email'],
          'status' => 'enabled',
          'selects' => 'user.*'
        ]);
        if ($user_data) {
          if ($post['password'] === $user_data['password']){
            $is_login = true;
          }
        }

        if ($is_login) {
          $this->luser->login($user_data);
          redirect('/', 'refresh');
        } else {
          $this->data['show_error'] = true;
        }
      }
    }
    $this->data['assets_id'] = 'login-index';
    $this->data['noindex'] = true;
    $this->data['title'] = 'ログイン';
    $this->load->view('user/login', $this->data);
  }

  /**
   * ログアウト
   *
   * @return void
   */
  public function logout()
  {
    $this->luser->logout();
    redirect('/login', 'refresh');
  }

  function _overlap_email($email)
  {
    if(!$email) return true;
    $this->db->select('seq');
    $user_data = $this->db->get_where('user', ['email' => $email])->result_array();
    if($user_data){
        $this->form_validation->set_message('_overlap_email', 'overlap');
        return false;
    }
    return true;
  }

  function _overlap_userid($user_id)
  {
    if(!$user_id) return true;
    $this->db->select('seq');
    $user_data = $this->db->get_where('user', ['id' => $user_id])->result_array();
    if($user_data){
        $this->form_validation->set_message('_overlap_userid', 'overlap');
        return false;
    }
    return true;
  }
}
