<?php

class Site extends Web_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('lsite_video');
  }

  public function index($page = 1)
  {
    // $params = [];
    // if ($action_status) $params['action_status'] = $action_status;
    // // TODO インフィニットにする
    // list($total, $this->data['qa_list']) = $this->lquestion->get_question_list($params, 1, 50);

    $params = [];
    list($total, $this->data['videos']) = $this->lsite_video->get_videos([], $page, PRE_PAGE);

    $this->data['is_h1'] = true;
    $this->data['title'] = $this->get_home_title();
    $this->data['pagination'] = get_pagination('/', $total, PRE_PAGE);
    $this->load->view('site/index', $this->data);
  }

  /**
   * 新着一覧
   *
   * @param integer $page
   * @return void
   */
  public function videos($page = 1)
  {
    list($total, $videos) = $this->lsite_video->get_videos([], $page, PRE_PAGE);
    $this->data['videos'] = $videos;
    $this->data['tag_name'] = '新着';
    $this->data['title'] = $this->get_page_title('新着動画');
    $this->data['pagination'] = get_pagination('/videos/', $total, PRE_PAGE);
    $this->load->view('site/search', $this->data);
  }

  /**
   * 動画タグ一覧
   *
   * @param [type] $tag_name
   * @param integer $page
   * @return void
   */
  public function videos_tag($tag_name = null, $page = 1)
  {
    if (!$tag_name) return $this->show_404();
    $tag_name = urldecode($tag_name);

    $params = ['tag_name' => $tag_name];
    list($total, $videos) = $this->lsite_video->get_videos($params, $page, PRE_PAGE);
    $this->data['videos'] = $videos;
    $this->data['tag_name'] = $tag_name;
    $this->data['title'] = $this->get_page_title($tag_name . '動画');
    $this->data['pagination'] = get_pagination("/videos/tag/{$tag_name}/", $total, PRE_PAGE);
    $this->load->view('site/search', $this->data);
  }

  public function video($video_id = null)
  {
    if (!$video_id) return $this->show_404();

    $this->data['video'] = $this->lsite_video->get_video(['video_id' => $video_id]);
    if (!$this->data['video']) return $this->show_404();

    $this->data['hide_videos_tag'] = 1;
    $this->data['videos'] = [];
    if ($this->data['video']['tags']) {
      $rand_key = array_rand($this->data['video']['tags'], 1);
      $params = ['tag_name' => $this->data['video']['tags'][$rand_key]];
      list($total, $this->data['videos']) = $this->lsite_video->get_videos($params, 1, 6);
    }

    $this->data['title'] = $this->get_page_title($this->data['video']['title']);
    $this->load->view('site/video', $this->data);
  }

  public function about()
  {
    $this->data['title'] = $this->get_page_title('運営者情報');
    $this->load->view('site/about', $this->data);
  }

  public function privacy_policy()
  {
    $this->data['title'] = $this->get_page_title('プライバシーポリシー');
    $this->load->view('site/privacy_policy', $this->data);
  }

  public function sitemap()
  {
    list($total, $this->data['videos']) = $this->lsite_video->get_videos([], 1, 1000);
    $this->load->view('site/sitemap', $this->data);
  }

  public function _404()
  {
    $this->show_404();
  }
}
