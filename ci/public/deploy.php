<?php
// https://cardinal:vw9whvvfpuDnwjT8X9VHwGr4AtDrieTk@保育士転職.net/deploy.php?key=bR5pZfvhVgcp
switch (true) {
  case !isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']):
  case $_SERVER['PHP_AUTH_USER'] !== 'cardinal':
  case $_SERVER['PHP_AUTH_PW']   !== 'vw9whvvfpuDnwjT8X9VHwGr4AtDrieTk':
    header('WWW-Authenticate: Basic realm="Enter username and password."');
    header('Content-Type: text/plain; charset=utf-8');
    die('Please login.');
}
header('Content-Type: text/html; charset=utf-8');

$params = $_GET;
if (!isset($params["key"]) || (isset($params["key"]) && $params["key"] !== "bR5pZfvhVgcp")) {
  echo "Not key";
  exit;
}

require "../vendor/autoload.php";
use phpseclib\Crypt\RSA;
use phpseclib\Net\SSH2;


$key = new RSA();
$key->loadKey(file_get_contents("/usr/local/yasunori.pem"));

$ssh = new SSH2('3.114.71.236');
if (!$ssh->login('ec2-user', $key)) {
  exit('Login Failed');
}

echo $ssh->exec('cd ~/yasunori && git pull > ~/checkout.log');
$ssh->disconnect();
