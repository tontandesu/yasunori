const gulpId = 'login-index'
const lib = require('../gulp-libraries')

const cssSetting = {
  name: gulpId,
  base: [],
  watch: ['scss/**', lib.path.libscss + '**'],
  import: ['scss/import.scss']
}

const jsSetting = {
  name: gulpId,
  base: [lib.path.jquery],
  watch: ['js/**'],
  import: [lib.path.commonJs + 'header_footer.js', 'js/index.js']
}

lib.setTasks({
  css: cssSetting,
  js: jsSetting
})
