const gulp = require('gulp')
const babel = require('gulp-babel')
const plumber = require('gulp-plumber')
const concat = require('gulp-concat')
const scss = require('gulp-sass')
const cleanCSS = require('gulp-clean-css')
const uglify = require('gulp-uglify')
const sourcemaps = require('gulp-sourcemaps')
const gulpif = require('gulp-if')
const convertEncoding = require('gulp-convert-encoding')
const path = require('path')
const webpackStream = require('webpack-stream')
const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')

let lib = {}
lib.isProduction = process.env.NODE_ENV === 'production'

lib.path = {}
lib.path.root = path.resolve(__dirname, './../') + '/'
lib.path.public = lib.path.root + 'public/'
lib.path.css = lib.path.public + 'css/'
lib.path.js = lib.path.public + 'js/'
lib.path.fonts = lib.path.public + 'fonts/'
lib.path.gulp = lib.path.root + 'gulp/'
lib.path.common = lib.path.gulp + 'common/'
lib.path.commonJs = lib.path.common + 'js/'
lib.path.commonScss = lib.path.common + 'scss/'
lib.path.nodeModules = lib.path.gulp + 'node_modules/'
lib.path.helper = lib.path.commonJs + 'helper.js'
lib.path.headerFooter = lib.path.commonJs + 'header_footer.js'
lib.path.normalizeCss =
  lib.path.nodeModules + 'normalize-scss/sass/_normalize.scss'
lib.path.jquery = lib.path.nodeModules + 'jquery/dist/jquery.js'
lib.path.jquerySlim = lib.path.nodeModules + 'jquery/dist/jquery.slim.js'
lib.path.ioniconsCss = lib.path.nodeModules + 'ionicons/dist/scss/ionicons.scss'
// lib.path.quillCoreCss = lib.path.nodeModules + 'quill/dist/quill.core.css'
// lib.path.quillSnowCss = lib.path.nodeModules + 'quill/dist/quill.snow.css'
lib.path.ioniconsFonts = lib.path.nodeModules + 'ionicons/dist/fonts/*'
lib.path.swiperJs = lib.path.nodeModules + 'swiper/dist/js/swiper.js'
lib.path.swiperCss = lib.path.nodeModules + 'swiper/dist/css/swiper.css'

const mode = lib.isProduction ? 'production' : 'development'

lib.jsSum = (settings) => {
  return gulp
    .src(settings.base.concat(settings.import))
    .pipe(plumber())
    .pipe(
      babel({
        presets: ['@babel/preset-env'],
        compact: lib.isProduction,
        ignore: [lib.path.swiperJs]
      })
    )
    .pipe(gulpif(!lib.isProduction, sourcemaps.init()))
    .pipe(concat(settings.name + '.js'))
    .pipe(gulpif(lib.isProduction, uglify()))
    .pipe(convertEncoding({ to: 'utf-8' }))
    .pipe(gulpif(!lib.isProduction, sourcemaps.write('./')))
    .pipe(gulp.dest(lib.path.js))
}

lib.cssSum = (settings) => {
  return gulp
    .src(settings.base.concat(settings.import))
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(scss())
    .pipe(concat(settings.name + '.css'))
    .pipe(gulpif(lib.isProduction, cleanCSS()))
    .pipe(convertEncoding({ to: 'utf-8' }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(lib.path.css))
}

lib.vueSum = (settings) => {
  let config = {
    mode: mode,
    performance: { hints: false },
    entry: settings.entry,
    output: {
      filename: `${settings.name}.js`,
      path: lib.path.js
    },
    cache: true,
    resolve: {
      extensions: ['.js', '.vue'],
      alias: {
        '@common_components': path.resolve(
          __dirname,
          '../gulp/common/vue/components'
        ),
        '@common': path.resolve(__dirname, '../gulp/common/vue'),
        '@class': path.resolve(__dirname, '../gulp/common/js/class'),
        vue$: 'vue/dist/vue.esm.js'
      }
    },
    plugins: [
      new VueLoaderPlugin(),
      new webpack.ProvidePlugin({
        $: 'jquery/dist/jquery.js'
        // $: "jquery/dist/jquery.slim.js",
      }),
      new webpack.DefinePlugin({
        __REACT_DEVTOOLS_GLOBAL_HOOK__: '({ isDisabled: true })'
      })
    ],
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: [
            { loader: 'babel-loader', options: { presets: ['@babel/preset-env'] } }
          ]
        },
        {
          test: /\.vue$/,
          exclude: /node_modules/,
          use: [{ loader: 'vue-loader' }]
        },
        // {test: /\.css$/, use: [{loader: "style-loader"}, {loader: "css-loader"}]},
        // {test: /\.(eot|ttf|woff|woff2)(\?\S*)?$/, use: [{loader: "file-loader"}]},
        { test: /\.svg$/, use: ['raw-loader'] },
        { test: /\.(html)$/, use: { loader: 'html-loader', options: { attrs: [':data-src'] } } },
        {
          test: /\.css$/,
          use: [
            {
              loader: 'style-loader',
              options: {
                singleton: true
              }
            },
            { loader: 'css-loader' }
          ]
        }
      ]
    }
  }

  if (!lib.isProduction) config.plugins.push(new HardSourceWebpackPlugin())

  return gulp
    .src(settings.entry)
    .pipe(plumber())
    .pipe(webpackStream(config, webpack))
    .pipe(gulp.dest(lib.path.js))

  // return webpackStream(config, webpack, () => {
  //     console.log('Webpack done!!');
  //   })
  //   .pipe(gulpif(lib.isProduction, uglify()))
  //   .pipe(gulp.dest(lib.path.js));
}

lib.copy = (done, settings) => {
  for (var i in settings) {
    var data = settings[i]
    gulp.src([data.from]).pipe(gulp.dest(data.to))
  }
  done()
}

lib.setTasks = (params) => {
  const cssSettings = params.css
  const jsSettings = params.js
  const vueSettings = params.vue
  const copySettings = params.copy
  let taskIds = []

  if (copySettings) {
    gulp.task('copy', (done) => lib.copy(done, copySettings))
    taskIds.push('copy')
  }

  if (cssSettings) {
    cssSettings.watch.unshift('../common/scss/**')
    gulp.task('cssSum', () => lib.cssSum(cssSettings))
    taskIds.push('cssSum')
  }

  if (jsSettings) {
    jsSettings.watch.unshift('../common/js/**')
    gulp.task('jsSum', () => lib.jsSum(jsSettings))
    taskIds.push('jsSum')
  }

  if (vueSettings) {
    // vueSettings.entry.unshift(lib.path.headerFooter);
    gulp.task('vueSum', () => lib.vueSum(vueSettings))
    taskIds.push('vueSum')
  }

  gulp.task('default', gulp.series(taskIds))
  // gulp.task('default', taskIds, () => console.log('complete!!'));

  gulp.task(
    'watch',
    gulp.series(['default'], () => {
      let jsWatchDir = ['js/**', '../common/js/**', '../common/vue/**']
      // let cssWatchDir = ['scss/**', '../common/scss/**'];
      if (cssSettings) gulp.watch(cssSettings.watch, gulp.series('cssSum'))
      if (jsSettings) gulp.watch(jsSettings.watch, gulp.series('jsSum'))
      if (vueSettings) gulp.watch(jsWatchDir, gulp.series('vueSum'))
    })
  )
}

module.exports = lib
