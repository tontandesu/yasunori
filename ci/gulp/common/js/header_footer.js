/* global $ */

let modalId = null

const getUrlParams = (search) => {
  if (!search) return {}
  let hashes = search.slice(search.indexOf('?') + 1).split('&')
  return hashes.reduce((params, hash) => {
    let [key, val] = hash.split('=')
    return Object.assign(params, { [key]: decodeURIComponent(val) })
  }, {})
}
const params = getUrlParams(window.location.search)

$(() => {
  setModalCloseEvent()

  $.each($('img._lazy'), (i, val) => {
    var $img = $(val)
    var url = $img.data('src')
    if (url) $img.attr('src', url)
  })

  $('img').on('error', () => {
    $(this).attr({ src: '/img/no.svg' })
  })

  $('a[href^="#"]').on('click', function(ev) {
    // ev.preventDefault();

    var speed = 400
    var href = $(this).attr('href')

    if (href == '#no') {
      return false
    }

    var target = $(href === '#' || href === '' ? 'html' : href)
    var position = target.offset().top

    $('body, html').animate({ scrollTop: position }, speed, 'swing')
    return false
  });


  (() => {
    const $share = $('._share_facebook')
    if (!$share.length) return
    $share.off().on('click', (ev) => {
      const { url, title } = beforeShareLink(ev, $(this))
      window.open(getShareLinkFacebook(url), '_blank')
    })
  })();

  (() => {
    const $share = $('._share_twitter')
    if (!$share.length) return
    $share.off().on('click', (ev) => {
      const { url, title } = beforeShareLink(ev, $share)
      window.open(getShareLinkTwitter(url, title), '_blank')
    })
  })();

  (() => {
    const $share = $('._share_line')
    if (!$share.length) return
    $share.off().on('click', (ev) => {
      const { url, title } = beforeShareLink(ev, $share)
      window.open(getShareLinkLine(url, title))
    })
  })();

  (() => {
    const $share = $('._share_hatena')
    if (!$share.length) return
    $share.off().on('click', (ev) => {
      const { url, title } = beforeShareLink(ev, $share)
      window.open(getShareLinkHatena(url))
    })
  })();

  (() => {
    const $header = $('#_header')
    const $question_btn = $('#_question_btn')
    $(window).on('scroll', () => {
      const top = $(window).scrollTop()
      if (top > 80) {
        $question_btn.addClass('out')
      } else {
        $question_btn.removeClass('out')
      }
      if (top > 40) {
        $header.addClass('onbg')
      } else {
        $header.removeClass('onbg')
      }
    })
  })();
})


const beforeShareLink = (ev) => {
  ev.preventDefault()
  const query = $(ev.currentTarget).data('query') || ''
  const url = encodeURIComponent(location.href + query)
  const title = encodeURIComponent($('title').text())
  return { url, title }
}

const getShareLinkFacebook = (url) => {
  return `https://www.facebook.com/sharer/sharer.php?u=${url}`
}
const getShareLinkTwitter = (url, title) => {
  return `https://twitter.com/share?url=${url}&text=${title}`
}
const getShareLinkLine = (url, title) => {
  return `line://msg/text/${title}%0D%0A${url}`
}
const getShareLinkHatena = (url) => {
  return `https://b.hatena.ne.jp/entry/${url}`
}

const dateFormat = (date, format) => {
  const _priority = ['yyyy', 'MM', 'dd', 'hh', 'mm', 'ss']
  const _fmt = {
    'yyyy': (date) => date.getFullYear() + '',
    'MM': (date) => ('0' + (date.getMonth() + 1)).slice(-2),
    'dd': (date) => ('0' + date.getDate()).slice(-2),
    'hh': (date) => ('0' + date.getHours()).slice(-2),
    'mm': (date) => ('0' + date.getMinutes()).slice(-2),
    'ss': (date) => ('0' + date.getSeconds()).slice(-2)
  }
  return _priority.reduce((res, fmt) => res.replace(fmt, _fmt[fmt](date)), format)
}

const displayTimeStamp = (timestamp) => {
  const date = new Date(timestamp)
  return dateFormat(date, 'yyyy/MM/dd hh:mm');
}

const displayTime = (unixTime) => {
  var date = new Date(unixTime.replace(/-/g, '/'))
  var diff = new Date().getTime() - date.getTime()
  var d = new Date(diff)

  if (d.getUTCFullYear() - 1970) {
    return d.getUTCFullYear() - 1970 + '年'
  } else if (d.getUTCMonth()) {
    return d.getUTCMonth() + 'ヶ月'
  } else if (d.getUTCDate() - 1) {
    return d.getUTCDate() - 1 + '日'
  } else if (d.getUTCHours()) {
    return d.getUTCHours() + '時間'
  } else if (d.getUTCMinutes()) {
    return d.getUTCMinutes() + '分'
  } else {
    return d.getUTCSeconds() + '秒'
  }
}

const setModalCloseEvent = () => {
  $('.xoverlay, .xmodal_close').click((ev) => {
    ev.preventDefault()
    closeModal()
  })
}
const closeModal = () => {
  $(`.xoverlay, ${modalId}`).hide()
  modalId = ''
}
