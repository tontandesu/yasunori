function set_daterangepicker(new_options, callback){
  var options = {
    startDate: undefined,
    endDate: undefined,
    minDate: undefined,
    maxDate: undefined,
    dateLimit: {days: 365},
    singleDatePicker: false,
    showDropdowns: true,
    opens: 'right',
    drops: 'down',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-primary',
    cancelClass: 'btn-default',
    separator: ' to ',
    locale: {
      format: config.date_format,
      applyLabel: lang.decision,
      cancelLabel: lang.cancel,
      fromLabel: 'From',
      toLabel: 'To',
      customRangeLabel: lang.custom,
      daysOfWeek: [lang.sun, lang.mon, lang.tues, lang.wed, lang.thu, lang.fri, lang.sat],
      monthNames: [lang.january, lang.february, lang.march, lang.april, lang.may, lang.june, lang.july, lang.august, lang.september, lang.october, lang.november, lang.december],
      firstDay: 1
    },
  };
  $.extend(options, new_options);
  // callback = function(start, end, label){}
  $("._daterangepicker").off().daterangepicker(options, callback);
};
