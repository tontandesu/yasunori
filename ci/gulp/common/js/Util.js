export default class Util {

  static getUrlParams(search) {
    if (!search) return {}
    let hashes = search.slice(search.indexOf('?') + 1).split('&')
    return hashes.reduce((params, hash) => {
      let [key, val] = hash.split('=')
      return Object.assign(params, {[key]: decodeURIComponent(val)})
    }, {})
  }
}

