export const deepCopy = (obj) => {
  return JSON.parse(JSON.stringify(obj))
}

export const getUrlParams = (search) => {
  if (!search) return {}
  const hashes = search.slice(search.indexOf('?') + 1).split('&')
  return hashes.reduce((params, hash) => {
    const [key, val] = hash.split('=')
    return { ...params, [key]: decodeURIComponent(val) }
  }, {})
}
