import axios from 'axios'
import humps from 'humps'

export default new class Xaxios {

  getCsrf () {
    return {
      key: document.querySelector(`meta[name="csrf-token-name"]`).getAttribute('content'),
      token: document.querySelector(`meta[name="csrf-token"]`).getAttribute('content')
    }
  }

  success (res) {}

  error (error) {
    console.error(error.response)
    alert("送信エラーが発生しました。\nお手数おかけしますが管理者までお問合せください。")
  }

  final () {}

  cleanObj (obj) {
    return Object.keys(obj).reduce((acc, key) => {
      if (obj[key] === null || obj[key] === undefined) return acc
      acc[key] = obj[key]
      return acc
    }, {})
  }

  get(url, params = {}, { success = this.success, error = this.error, final = this.final }) {
    axios.get(url, { params: this.cleanObj(params) })
      .then((res) => success(humps.camelizeKeys(res.data)))
      .catch((res) => error(res))
      .finally(() => final())
  }

  post(url, params = {}, { success = this.success, error = this.error, final = this.final }) {
    const csrf = this.getCsrf()
    const values = new URLSearchParams(humps.decamelizeKeys(this.cleanObj(params)))
    values.append(csrf.key, csrf.token)
    axios.post(url, values, {})
      .then((res) => success(humps.camelizeKeys(res.data)))
      .catch((res) => error(res))
      .finally(() => final())
  }

  postContents(url, file, { params = {}, success = this.success, error = this.error, final = this.final }) {
    const data = new FormData()
    data.append('userfile', file)

    const _params = humps.decamelizeKeys(this.cleanObj(params))
    for (const key in _params) {
      data.append(key, _params[key])
    }

    const csrf = this.getCsrf()
    data.append(csrf.key, csrf.token)
    axios.post(url, data, {})
      .then((res) => success(humps.camelizeKeys(res.data)))
      .catch((res) => error(res))
      .finally(() => final())
  }

  delete(url, params = {}, { success = this.success, error = this.error, final = this.final }) {
    axios.delete(url, this.cleanObj(params))
      .then((res) => success(humps.camelizeKeys(res.data)))
      .catch((res) => error(res))
      .finally(() => final())
  }
}
