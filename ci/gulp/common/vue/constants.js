export const CSRF = {
  key: document
    .querySelector(`meta[name="csrf-token-name"]`)
    .getAttribute('content'),
  token: document
    .querySelector(`meta[name="csrf-token"]`)
    .getAttribute('content')
}

export const PASSWORD_RULE = {
  min: 6,
  max:20,
  regex: /^(?=.*?[a-z])(?=.*?\d)[a-z\d]{6,20}$/i
}
