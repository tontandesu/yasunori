const gulpId = 'admin-index'
const lib = require('../gulp-libraries')

const cssSetting = {
  name: gulpId,
  base: [lib.path.ioniconsCss],
  watch: ['scss/**', lib.path.libscss + '**'],
  import: ['scss/import.scss']
}

let vueSettings = {
  name: gulpId,
  entry: ['./js/index.js']
}

const copySetting = [
  {
    from: lib.path.ioniconsFonts,
    to: lib.path.fonts
  }
]

lib.setTasks({
  css: cssSetting,
  vue: vueSettings,
  copy: copySetting
})
