import { mapState, mapGetters, mapMutations, mapActions } from 'vuex'
import { deepCopy } from '@class/Util'
import Xaxios from '@class/Xaxios'

const initialState = {
  isFetching: false,
  videos: []
}
const state = deepCopy(initialState)
const getters = {}
const mutations = {
  updateIsFetching(state, value) {
    state.isFetching = value
  },
  updateVideos(state, videos) {
    state.videos = videos
  }
}
const actions = {
  fetchVideos ({ state, commit }, params = {}) {
    commit('updateIsFetching', true)
    const success = ({ total, list }) => {
      // TODO: 検索 or ページング入れる
      commit('updateVideos', list)
    }
    const final = () => {
      commit('updateIsFetching', false)
    }
    Xaxios.get('/api/videos', params, { success, final })
  }
}
export const videosMixin = {
  computed: {
    ...mapState('videos', Object.keys(state)),
    ...mapGetters('videos', Object.keys(getters))
  },
  methods: {
    ...mapMutations('videos', Object.keys(mutations)),
    ...mapActions('videos', Object.keys(actions))
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
