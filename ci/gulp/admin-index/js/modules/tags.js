import { mapState, mapGetters, mapMutations, mapActions } from 'vuex'
import { deepCopy } from '@class/Util'
import Xaxios from '@class/Xaxios'

const initialtag = {
  seq: '',
  name: '',
  kana: '',
  recommend: 0
}

const initialState = {
  isFetching: false,
  tags: []
}
const state = deepCopy(initialState)
const getters = {}
const mutations = {
  updateIsFetching(state, value) {
    state.isFetching = value
  },
  updateTags(state, tags) {
    state.tags = tags
  },
  updateTagSeq(state, { index, seq }) {
    state.tags[index]['seq'] = seq
  },
  addTag(state) {
    state.tags.unshift({ ...initialtag })
  },
  deleteTag(state, index) {
    state.tags.splice(index, 1)
  }
}
const actions = {
  fetchTags ({ state, commit }, params = {}) {
    commit('updateIsFetching', true)
    const success = (list) => {
      commit('updateTags', list)
    }
    const final = () => {
      commit('updateIsFetching', false)
    }
    Xaxios.get('/api/tags', params, { success, final })
  },
  saveTag ({ state, commit, getters }, index) {
    const success = (data) => {
      commit('updateTagSeq', { index, seq: data.seq })
    }
    // TODO
    const error = ({ response }) => {
      console.log(response)
      // if (response.status === 400) {
      //   alert(Object.values(response.data).join("\n"))
      //   return
      // }
    }
    const params = state.tags[index]
    Xaxios.post('/api/tag', params, { success, error })
  },
  deleteTag ({ state, commit }, { index, tagSeq }) {
    if (!tagSeq) {
      commit('deleteTag', index)
      return
    }
    commit('updateIsSaveing', true)
    const success = (data) => {
      commit('deleteTag', index)
    }
    const final = () => {
      commit('updateIsSaveing', false)
    }
    Xaxios.delete(`/api/tag/${tagSeq}`, {}, { success, final })
  },
}
export const tagsMixin = {
  computed: {
    ...mapState('tags', Object.keys(state)),
    ...mapGetters('tags', Object.keys(getters))
  },
  methods: {
    ...mapMutations('tags', Object.keys(mutations)),
    ...mapActions('tags', Object.keys(actions))
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
