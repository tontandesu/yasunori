import { mapState, mapGetters, mapMutations, mapActions } from 'vuex'
import { deepCopy } from '@class/Util'
import Xaxios from '@class/Xaxios'

const initialState = {
  isPageLoading: true,
  currentUser: {
    seq: null,
    id: null,
    email: null,
    permission: null,
    icon: null,
    message: null,
    displayName: null
  }
}
const state = deepCopy(initialState)

const getters = {}

const mutations = {
  setIsPageLoading (state, value) {
    state.isPageLoading = value
  },
  updateCurrentUser (state, data) {
    const currentUser = { ...initialState.currentUser }
    for (const key in currentUser) {
      currentUser[key] = data[key] === undefined ? currentUser[key] : data[key]
    }
    state.currentUser = currentUser
  }
}

const actions = {
  fetchCurrentUser ({ state, commit, dispatch }, { seq, type } = {}) {
    const success = (data) => {
      commit('updateCurrentUser', data)
    }
    Xaxios.get('/api/user', {}, { success })
  }
}

export const appMixin = {
  computed: {
    ...mapState('app', Object.keys(state)),
    ...mapGetters('app', Object.keys(getters))
  },
  methods: {
    ...mapMutations('app', Object.keys(mutations)),
    ...mapActions('app', Object.keys(actions))
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
