import { mapState, mapGetters, mapMutations, mapActions } from 'vuex'
import { deepCopy } from '@class/Util'
import Xaxios from '@class/Xaxios'

const initialVideo = {
  videoSeq: '',
  title: '',
  description: '',
  keywords: '',
  url: null,
  comment: '',
  playCount: 0,
  goodCount: 0,
  badCount: 0,
  done: 0,
  isShow: 1
}

const initialState = {
  isFetching: false,
  isSaveing: false,
  site: {},
  videos: []
}
const state = deepCopy(initialState)
const getters = {}
const mutations = {
  updateSite(state, data) {
    state.site = data
  },
  updateVideos(state, videos) {
    state.videos = videos
  },
  updateIsFetching(state, value) {
    state.isFetching = value
  },
  toggleItem(state, index) {
    state.videos[index].isShow = !state.videos[index].isShow
  }
}
const actions = {
  syncVideos ({ state, commit, dispatch }) {
    commit('updateIsFetching', true)
    const success = (res) => {
      dispatch('fetchVideos')
    }
    const final = () => {
      commit('updateIsFetching', false)
    }
    const seq = state.site.seq
    Xaxios.post('/api/site/videos-sync', { seq }, { success, final })
  },
  deleteVideos ({ state, commit, dispatch }) {
    commit('updateIsFetching', true)
    const success = (res) => {
      dispatch('fetchVideos')
    }
    const final = () => {
      commit('updateIsFetching', false)
    }
    const seq = state.site.seq
    Xaxios.post('/api/site/delete-videos', { seq }, { success, final })
  },
  fetchVideos ({ state, commit }, params = {}) {
    commit('updateIsFetching', true)
    const success = ({ total, list }) => {
      const _list = list.map((item) => {
        item.isShow = !item.done
        return item
      })
      commit('updateVideos', _list)
    }
    const final = () => {
      commit('updateIsFetching', false)
    }
    console.log(params)
    Xaxios.get('/api/site/videos', params, { success, final })
  },
  fetchSite ({ state, commit }, siteSeq) {
    commit('updateIsFetching', true)
    const success = (data) => {
      commit('updateSite', data)
    }
    const final = () => {
      commit('updateIsFetching', false)
    }
    Xaxios.get(`/api/site/${siteSeq}`, {}, { success, final })
  },
  saveVideo ({ state, commit, getters }, index) {
    const success = (data) => {}
    // TODO
    const error = ({ response }) => {
      console.log(response)
      if (response.status === 400) {
        alert(Object.values(response.data).join("\n"))
        return
      }
    }
    const params = state.videos[index]
    Xaxios.post('/api/site/video', params, { success, error })
  }
}
export const siteVideosMixin = {
  computed: {
    ...mapState('siteVideos', Object.keys(state)),
    ...mapGetters('siteVideos', Object.keys(getters))
  },
  methods: {
    ...mapMutations('siteVideos', Object.keys(mutations)),
    ...mapActions('siteVideos', Object.keys(actions))
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
