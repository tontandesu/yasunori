import Vue from 'vue'
import Vuex from 'vuex'

import app from './app'
import siteInput from './site-input'
import siteVideos from './site-videos'
import sites from './sites'
import videoInput from './video-input'
import videos from './videos'
import tags from './tags'

Vue.use(Vuex)
export default new Vuex.Store({
  modules: {
    app,
    siteInput,
    siteVideos,
    sites,
    videoInput,
    videos,
    tags
  }
})
