import { mapState, mapGetters, mapMutations, mapActions } from 'vuex'
import { deepCopy } from '@class/Util'
import Xaxios from '@class/Xaxios'

const initialState = {
  isFetching: false,
  isSaveing: false,
  isEdit: false,
  themes: [],
  form: {
    seq: '',
    domain: '',
    name: '',
    kana: '',
    copy: '',
    description: '',
    keywords: '',
    eyecatch: '',
    theme: '',
    script: '',
    status: ''
  },
}
const state = deepCopy(initialState)
const getters = {
  saveParams(state) {
    const params = deepCopy(state.form)
    params.status = params.status ? 'enabled' : 'disabled'
    return params
  }
}
const mutations = {
  initForm(state) {
    state.form = { ...initialState.form }
  },
  updateValue(state, { key, value }) {
    state[key] = value
  },
  updateFormValue(state, { key, value }) {
    state.form[key] = value
  },
  updateIsFetching(state, value) {
    state.isFetching = value
  },
  updateIsSaveing(state, value) {
    state.isSaveing = value
  },
  updateEyecatch(state, url) {
    state.form.eyecatch = url
  },
  setStatusDraft(state) {
    state.form.status = 'draft'
  },
  setStatusLimited(state) {
    state.form.status = 'limited'
  },
  setStatusRelease(state) {
    state.form.status = 'release'
  },
}
const actions = {
  deleteSite ({ state, commit }, { complete }) {
    commit('updateIsSaveing', true)
    const success = (data) => {
      complete(data)
    }
    const final = () => {
      commit('updateIsSaveing', false)
    }
    Xaxios.delete(`/api/site/${state.form.seq}`, {}, { success, final })
  },
  fetchThemes ({ state, commit }) {
    const success = (value) => {
      commit('updateValue', { key: 'themes', value })
    }
    Xaxios.get(`/api/themes`, {}, { success })
  },
  fetchSite ({ state, commit }, { siteSeq }) {
    commit('updateIsFetching', true)
    const success = (data) => {
      const value = Object.keys(state.form).reduce((acc, key) => {
        return { ...acc, [key]: data[key] }
      }, {})
      commit('updateValue', { key: 'form', value })
      commit('updateValue', { key: 'isEdit', value: true })
    }
    const final = () => {
      commit('updateIsFetching', false)
    }
    Xaxios.get(`/api/site/${siteSeq}`, {}, { success, final })
  },
  saveSite ({ state, commit, getters }, { complete }) {
    commit('updateIsSaveing', true)
    const success = (data) => {
      complete(data)
      commit('initForm')
    }
    const error = ({ response }) => {
      console.log(response)
      if (response.status === 400) {
        alert(Object.values(response.data).join("\n"))
        return
      }
    }
    const final = () => {
      commit('updateIsSaveing', false)
    }
    Xaxios.post('/api/site', getters.saveParams, { success, error, final })
  }
}
export const siteInputMixin = {
  computed: {
    ...mapState('siteInput', Object.keys(state)),
    ...mapGetters('siteInput', Object.keys(getters))
  },
  methods: {
    ...mapMutations('siteInput', Object.keys(mutations)),
    ...mapActions('siteInput', Object.keys(actions))
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
