import { mapState, mapGetters, mapMutations, mapActions } from 'vuex'
import { deepCopy } from '@class/Util'
import Xaxios from '@class/Xaxios'

const initialState = {
  isFetching: false,
  isSaveing: false,
  isEdit: false,
  themes: [],
  tags: [],
  form: {
    seq: '',
    id: '',
    title: '',
    description: '',
    keywords: '',
    url: '',
    eyecatch: '',
    comment: '',
    playCount: '',
    goodCount: '',
    badCount: '',
    tags: [],
    status: ''
  },
}
const state = deepCopy(initialState)
const getters = {
  saveParams(state) {
    const params = deepCopy(state.form)
    params.status = params.status ? 'enabled' : 'disabled'
    return params
  }
}
const mutations = {
  initForm(state) {
    state.form = { ...initialState.form }
  },
  updateValue(state, { key, value }) {
    state[key] = value
  },
  updateFormValue(state, { key, value }) {
    state.form[key] = value
  },
  updateIsFetching(state, value) {
    state.isFetching = value
  },
  updateIsSaveing(state, value) {
    state.isSaveing = value
  },
  updateEyecatch(state, url) {
    // state.eyecatchUrl = url
    state.form.eyecatch = url
  },
}
const actions = {
  deleteVideo ({ state, commit }, { complete }) {
    commit('updateIsSaveing', true)
    const success = (data) => {
      complete(data)
    }
    const final = () => {
      commit('updateIsSaveing', false)
    }
    Xaxios.delete(`/api/video/${state.form.seq}`, {}, { success, final })
  },
  fetchTags ({ state, commit }) {
    const success = (value) => {
      commit('updateValue', { key: 'tags', value })
    }
    Xaxios.get(`/api/tags`, {}, { success })
  },
  fetchVideo ({ state, commit }, { videoSeq }) {
    commit('updateIsFetching', true)
    const success = (data) => {
      const value = Object.keys(state.form).reduce((acc, key) => {
        if (key === 'tags') {
          return { ...acc, tags: data.tags.map(item => item.seq) }
        }
        return { ...acc, [key]: data[key] }
      }, {})
      console.log(value)
      commit('updateValue', { key: 'form', value })
      commit('updateValue', { key: 'isEdit', value: true })
    }
    const final = () => {
      commit('updateIsFetching', false)
    }
    Xaxios.get(`/api/video/${videoSeq}`, {}, { success, final })
  },
  saveVideo ({ state, commit, getters }, { complete }) {
    commit('updateIsSaveing', true)
    const success = (data) => {
      complete(data)
      commit('initForm')
    }
    const error = ({ response }) => {
      console.log(response)
      if (response.status === 400) {
        alert(Object.values(response.data).join("\n"))
        return
      }
    }
    const final = () => {
      commit('updateIsSaveing', false)
    }
    Xaxios.post('/api/video', getters.saveParams, { success, error, final })
  }
}
export const videoInputMixin = {
  computed: {
    ...mapState('videoInput', Object.keys(state)),
    ...mapGetters('videoInput', Object.keys(getters))
  },
  methods: {
    ...mapMutations('videoInput', Object.keys(mutations)),
    ...mapActions('videoInput', Object.keys(actions))
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
