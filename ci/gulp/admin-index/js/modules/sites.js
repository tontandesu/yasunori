import { mapState, mapGetters, mapMutations, mapActions } from 'vuex'
import { deepCopy } from '@class/Util'
import Xaxios from '@class/Xaxios'

const initialState = {
  isFetching: false,
  sites: []
}
const state = deepCopy(initialState)
const getters = {}
const mutations = {
  updateIsFetching(state, value) {
    state.isFetching = value
  },
  updateSites(state, sites) {
    state.sites = sites
  }
}
const actions = {
  fetchSites ({ state, commit }, params = {}) {
    commit('updateIsFetching', true)
    const success = ({ total, list }) => {
      commit('updateSites', list)
    }
    const final = () => {
      commit('updateIsFetching', false)
    }
    Xaxios.get('/api/sites', params, { success, final })
  }
}
export const sitesMixin = {
  computed: {
    ...mapState('sites', Object.keys(state)),
    ...mapGetters('sites', Object.keys(getters))
  },
  methods: {
    ...mapMutations('sites', Object.keys(mutations)),
    ...mapActions('sites', Object.keys(actions))
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
