import Vue from 'vue'
import VeeValidate from 'vee-validate'
import ja from 'vee-validate/dist/locale/ja'
import router from './router'
import store from './modules'
import App from './views/App'

Vue.config.productionTip = false
Vue.use(VeeValidate, {
  locale: 'ja',
  dictionary: { ja: ja }
})
const app = new Vue({
  router,
  store,
  render: (h) => h(App)
})
app.$mount('#app')
