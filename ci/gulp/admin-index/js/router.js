import Vue from 'vue'
import Router from 'vue-router'
import Sites from './views/Sites'
import SitesInput from './views/SitesInput'
import SiteVideos from './views/SiteVideos'
import Tags from './views/Tags'
import Videos from './views/Videos'
import VideoInput from './views/VideoInput'

Vue.use(Router)
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Sites
    },
    {
      path: '/sites',
      name: 'sites',
      component: Sites
    },
    {
      path: '/site/input',
      name: 'site-input',
      component: SitesInput
    },
    {
      path: '/site/input/:seq',
      name: 'site-edit',
      component: SitesInput
    },
    {
      path: '/site/videos/:seq',
      name: 'site-videos',
      component: SiteVideos
    },
    {
      path: '/tags',
      name: 'tags',
      component: Tags
    },
    {
      path: '/videos',
      name: 'videos',
      component: Videos
    },
    {
      path: '/video/input',
      name: 'video-input',
      component: VideoInput
    },
    {
      path: '/video/input/:seq',
      name: 'video-edit',
      component: VideoInput
    }
  ]
})
