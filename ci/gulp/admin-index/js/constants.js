export const CSRF = {
  key: document
    .querySelector(`meta[name="csrf-token-name"]`)
    .getAttribute('content'),
  token: document
    .querySelector(`meta[name="csrf-token"]`)
    .getAttribute('content')
}
