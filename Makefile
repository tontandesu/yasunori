build:
ifeq ($(env),production)
	cp mysql/env.production mysql/.env
	cp nginx/env.production nginx/.env

	sudo chmod -R 777 ~/yasunori/mysql/data/
	sudo chmod -R 777 ~/yasunori/ci/application/logs/
	sudo chmod -R 777 ~/yasunori/ci/public/
	sudo cp scripts/wakeup /etc/init.d
	sudo chkconfig --add wakeup
	sudo chkconfig wakeup on

	sudo cp /etc/localtime /etc/localtime.org
	sudo ln -sf  /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
	@make cron
else
	# docker stop $(docker ps -a -q)
	cp mysql/env.local mysql/.env
	cp nginx/env.local nginx/.env
endif
	docker-compose build
	docker-compose up -d

	sleep 60
	@make gulp
	@make composer

rebuild:
	docker-compose down
	# docker stop $(dockerps_all)
	# docker-compose rm -f
	@make build

cron:
	sudo cp scripts/crontab /var/spool/cron/ec2-user

composer:
	docker-compose exec ci bash -c " \
		composer install \
	"

gulp:
	docker-compose exec ci bash -c " \
		npm install --prefix /webapp/gulp --unsafe-perm=true \
		&& cd /webapp/gulp/admin-index && npx gulp \
		&& cd /webapp/gulp/login-index && npx gulp \
		&& cd /webapp/gulp/sky-site && npx gulp \
	"

start:
	docker-compose stop
	docker-compose up -d
	sudo systemctl restart crond.service

.PHONY: start
