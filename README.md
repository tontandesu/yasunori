# YASUNORI（アダルトCMS）

## ssh 接続

chmod 400 ~/GoogleDrive/work/aws/yasunori.pem  
ssh -i "~/GoogleDrive/work/aws/yasunori.pem" ec2-user@52.68.194.249

## 本番デプロイ方法

master ブランチにマージすると自動的に本番サーバーで git pull が実行される  
その他は手動実行

```
make cron
make composer
make gulp
```

## EC2 インスタンス構築手順

### bit とインスタンスを繋げる

1. 公開鍵の取得

```
ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/ec2-user/.ssh/id_rsa): #空Enter
Enter passphrase (empty for no passphrase): #空Enter
Enter same passphrase again: #空Enter

chmod 600 ~/.ssh/id_rsa
cat ~/.ssh/id_rsa.pub
# 内容コピー
```

2. bitbucket に鍵を登録
   「対象リポジトリ > 設定 > アクセスキー」で鍵を追加します。

3. インスタンス内にて…

```
sudo yum install -y git
git clone git@bitbucket.org:tontandesu/yasunori.git
```

4. Deploy

```
# dockerインストール
sudo amazon-linux-extras install docker -y
sudo groupadd docker
sudo usermod -a -G docker $USER && sudo gpasswd -a $USER docker && sudo systemctl start docker && sudo chkconfig docker on && exit

# 一度ログアウトされるので再度接続
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
cd ~/yasunori
make build env=production
```

5. bitbucket にトリガーを登録
   設定 > Webhooks の URL に以下を入力し保存

```
https://cardinal:vw9whvvfpuDnwjT8X9VHwGr4AtDrieTk@m-shovel.com/deploy.php?key=bR5pZfvhVgcp
```

6. cron 自動起動設定

```
systemctl status crond.service
sudo systemctl enable crond.service
sudo systemctl restart crond.service
```
